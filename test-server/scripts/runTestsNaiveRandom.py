#!/usr/bin/python
import os
import sys
import time
from os import path
from subprocess import call

def main(numTests, prob):
    os.chdir("..")

    resultFile = "result" + "P" + prob
    print(resultFile)
    start_all = time.time()
    for i in range(1, int(numTests)+1):
        print("Running test %s" % i)
        #startB = time.time()

        seed = i + 12345678
        #seed = 12346496
        call("mvn {0} {1} {2}".format("exec:java", "-Dexec.mainClass=explorer.SystemRunner", "-Dexec.args=\"randomSeed={0} dropWithProb={1} resultFile={2}\" ".format(str(seed), str(prob), resultFile)), shell=True)

        #endB = time.time()
        #elapsedSec = endB - startB
        #print("All Seconds for test %s: %s" % (i, elapsedSec))
        #print("All Minutes for test %s: %s" % (i, elapsedSec / 60))

    endAll = time.time()
    elapsedSec = endAll - start_all
    secs = "All Seconds for all tests: {}".format(elapsedSec)
    mins = "All Minutes for all tests: {}".format(elapsedSec / 60)

    print(secs)
    print(mins)

    f = open(resultFile, 'a')
    f.write(secs + "\n" + mins + "\n")
    f.close()


if __name__ == '__main__':
     numtests = 1
     if len(sys.argv) != 3:
        print("Please enter the parameter for: numtests, dropWithProb")
        print("Example usage: python runtestsNaiveRandom.py 10 0.35 ")
     else:
        main(sys.argv[1], sys.argv[2])
