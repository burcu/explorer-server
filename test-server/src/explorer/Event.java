package explorer;

import explorer.message.MessageComparator;
import explorer.message.PaxosMessage;
import explorer.scheduler.VectorClock;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Event {

  private final PaxosMessage message;
  private final VectorClock vc;

  public Event(PaxosMessage message, VectorClock vc) {
    this.message = message;
    this.vc = vc;
  }

  public PaxosMessage getMessage() {
    return message;
  }

  public VectorClock getVc() {
    return vc;
  }

  public boolean isConcurrentTo(Event other) {
    return this.vc.isConcurrent(other.vc);
  }

  // to iterate over a set of added messages in the same order across runs
  public static void sortEvents(List<Event> events) {
    // sort transitions by event id
    MessageComparator messageComparator = new MessageComparator();
    events.sort(Comparator.comparing(Event::getMessage, messageComparator));
  }
}
