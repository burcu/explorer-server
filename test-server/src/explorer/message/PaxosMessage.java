package explorer.message;

import com.google.gson.Gson;
import explorer.message.payload.AckPayload;
import explorer.message.payload.Payload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;

public class PaxosMessage {
  private static final Logger log = LoggerFactory.getLogger(PaxosMessage.class);
  public int seqNumber; // to identify between same sender-receiver messages, insertion id
  private final long sender;
  private final long recv;
  private final String verb;
  private final String usrval;

  private final String payload;
  private Payload payloadObj; // Needs to be accessed by its getter method
  private String ballot = ""; // The ballot this message is associated to - Externally set

  public enum ProtocolRound {PAXOS_PREPARE, PAXOS_PREPARE_RESPONSE, PAXOS_PROPOSE, PAXOS_PROPOSE_RESPONSE, PAXOS_COMMIT, PAXOS_COMMIT_RESPONSE};

  public PaxosMessage(int sequenceNumber, long sender, long recv, String verb, String payload, String usrval) {
    this.sender = sender;
    this.recv = recv;
    this.verb = verb;
    this.payload = payload;
    this.usrval = usrval;
    this.seqNumber = sequenceNumber;

    // Gson does not create this, so check in getPayloadObject method
    this.payloadObj = Payload.createPayload(getProtocolStep(), this.payload);
  }

  public void setSeqNumber(int seqNumber) {
    this.seqNumber = seqNumber;
  }

  public int getSeqNumber() {
    return seqNumber;
  }

  public long getSender() {
    return sender;
  }

  public long getRecv() {
    return recv;
  }

  public String getVerb() {
    return verb;
  }

  public String getPayload() {
    return payload;
  }

  public Payload getPayloadObj() {
    if(payloadObj == null) payloadObj = Payload.createPayload(getProtocolStep(), this.payload);
    return payloadObj;
  }

  public String getUsrval() {
    return usrval;
  }

  public String getPayloadBallot() {
    return getPayloadObj().getBallot();
  }

  public boolean hasSmallerBallot(PaxosMessage other) {
    if(ballot.equals("") || other.ballot.equals("")) {
      log.error("No ballot is set for the Paxos message.");
      System.exit(-1);
    }

    assert ballot.length() > 6 && other.ballot.length() > 6;

    return Integer.parseInt(ballot.substring(0, 6), 16) < Integer.parseInt(other.ballot.substring(0, 6), 16);
  }

  public static boolean isSmallerBallot(String ballot1, String ballot2) {
    if(ballot1.equals("") || ballot2.equals("")) {
      log.error("No ballot is set for the Paxos message.");
      System.exit(-1);
    }
    assert ballot1.length() > 6 && ballot2.length() > 6;

    return Integer.parseInt(ballot1.substring(0, 6), 16) < Integer.parseInt(ballot2.substring(0, 6), 16);
  }

  public int getProtocolStep() {
    switch (verb) {
      case "PAXOS_PREPARE":
        return 0;
      case "PAXOS_PREPARE_RESPONSE":
        return 1;
      case "PAXOS_PROPOSE":
        return 2;
      case "PAXOS_PROPOSE_RESPONSE":
        return 3;
      case "PAXOS_COMMIT":
        return 4;
      case "PAXOS_COMMIT_RESPONSE":
        return 5;
    }
      return -1;
  }

  public int getClientRequest() {
    if(verb.equals("PAXOS_PREPARE") || verb.equals("PAXOS_PROPOSE") || verb.equals("PAXOS_COMMIT")) {
      return (int)sender;
    } if(verb.equals("PAXOS_PREPARE_RESPONSE") || verb.equals("PAXOS_PROPOSE_RESPONSE") || verb.equals("PAXOS_COMMIT_RESPONSE")) {
      return (int)recv;
    }
    return -1;
  }

  public boolean isRequest() {
    return verb.equals("PAXOS_PREPARE") || verb.equals("PAXOS_PROPOSE") || verb.equals("PAXOS_COMMIT");
  }

  public boolean isResponse() {
    return verb.equals("PAXOS_PREPARE_RESPONSE") || verb.equals("PAXOS_PROPOSE_RESPONSE") || verb.equals("PAXOS_COMMIT_RESPONSE");
  }

  public boolean isResponseOf(PaxosMessage m) {
    if(verb.equals("PAXOS_PREPARE_RESPONSE") && m.verb.equals("PAXOS_PREPARE") && this.sender == m.recv && this.recv == m.sender) {
      return true;
    } else if(verb.equals("PAXOS_PROPOSE_RESPONSE") && m.verb.equals("PAXOS_PROPOSE") && this.sender == m.recv && this.recv == m.sender) {
      return true;
    } else if(verb.equals("PAXOS_COMMIT_RESPONSE") && m.verb.equals("PAXOS_COMMIT") && this.sender == m.recv && this.recv == m.sender) {
      return true;
    }
    return false;
  }

  public static String toJsonStr(PaxosMessage obj) {
    Gson gson = new Gson();
    //System.out.println(gson.toJson(obj));
    return gson.toJson(obj);
  }

  public static PaxosMessage toObject(String json) {
    Gson gson = new Gson();
    //System.out.println(json);
    PaxosMessage temp = gson.fromJson(json, PaxosMessage.class);
    return temp;
  }

  public boolean isAckEvent() {
    return getPayloadObj() instanceof AckPayload;
  }


  public String getBallot() {
    return ballot;
  }

  public void setBallot(String b) {
    ballot = b;
  }

  @Override
  public String toString() {
    return "Req: " + getClientRequest() + " Ballot: " + getBallot() + " Protocol step: " + verb +
        " From: " + sender + " To: " + recv + " - " + verb;
  }

  public String getMessageContentWithBallot() {
    StringBuilder sb = new StringBuilder("Req-");
    sb.append(getClientRequest());
    sb.append("--");
    sb.append("Ballot: ");
    sb.append(getBallot());
    sb.append("--");
    sb.append(getVerb());
    sb.append("--From-");
    sb.append(getSender());
    sb.append("--To-");
    sb.append(getRecv());
    return sb.toString();
  }

  public String getMessageContent() {
    StringBuilder sb = new StringBuilder("Req-");
    sb.append(getClientRequest());
    sb.append("--");
    sb.append(getVerb());
    sb.append("--From-");
    sb.append(getSender());
    sb.append("--To-");
    sb.append(getRecv());
    return sb.toString();
  }

  public String getMessageContentWithSeqNumber() {
    return "#" + seqNumber + "--" + getMessageContentWithBallot();
  }

  public boolean hasSameContent(PaxosMessage other) {
    return getClientRequest() == other.getClientRequest()
            && getVerb().equals(other.getVerb())
            && getSender() == other.getSender()
            && getRecv() == other.getRecv();
  }

  public boolean hasSameContentAndBallot(PaxosMessage other) {
    return ballot.equals(other.ballot) && hasSameContent(other);
  }

  public static boolean hasSameContent(PaxosMessage m1, PaxosMessage m2) {
    return m1.getClientRequest() == m2.getClientRequest()
        && m1.getVerb().equals(m2.getVerb())
        && m1.getSender() == m2.getSender()
        && m1.getRecv() == m2.getRecv();
  }

  public boolean hasSameClientRequest(PaxosMessage other) {
    return this.getClientRequest() == other.getClientRequest();
  }

  public boolean hasSameSRPair(PaxosMessage other) {
    return this.getSender() == other.getSender() && this.getRecv() == other.getRecv();
  }

  @Override
  public boolean equals(Object obj) {
    if(obj == this) return true;

    if(!(obj instanceof PaxosMessage)) return false;

    PaxosMessage e = (PaxosMessage) obj;
    return seqNumber == ((PaxosMessage) obj).seqNumber &&
        sender == e.sender  &&
        recv == e.recv &&
        verb.equals(e.verb) &&
        payload.equals(e.payload) &&
        usrval.equals(e.usrval);
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (int)sender;
    result = 31 * result + verb.hashCode();
    result = 31 * result + (int)recv;
    result = 31 * result + payload.hashCode();
    result = 31 * result + usrval.hashCode();
    return result;
  }

  public static List<String> messageIds = Arrays.asList(
      "Req-0--PAXOS_PREPARE--From-0--To-0",
      "Req-0--PAXOS_PREPARE--From-0--To-1",
      "Req-0--PAXOS_PREPARE--From-0--To-2",
      "Req-0--PAXOS_PREPARE_RESPONSE--From-0--To-0",
      "Req-0--PAXOS_PREPARE_RESPONSE--From-1--To-0",
      "Req-0--PAXOS_PREPARE_RESPONSE--From-2--To-0",
      "Req-0--PAXOS_PROPOSE--From-0--To-0",
      "Req-0--PAXOS_PROPOSE--From-0--To-1",
      "Req-0--PAXOS_PROPOSE--From-0--To-2",
      "Req-0--PAXOS_PROPOSE_RESPONSE--From-0--To-0",
      "Req-0--PAXOS_PROPOSE_RESPONSE--From-1--To-0",
      "Req-0--PAXOS_PROPOSE_RESPONSE--From-2--To-0",
      "Req-0--PAXOS_COMMIT--From-0--To-0",
      "Req-0--PAXOS_COMMIT--From-0--To-1",
      "Req-0--PAXOS_COMMIT--From-0--To-2",
      "Req-0--PAXOS_COMMIT_RESPONSE--From-0--To-0",
      "Req-0--PAXOS_COMMIT_RESPONSE--From-1--To-0",
      "Req-0--PAXOS_COMMIT_RESPONSE--From-2--To-0",

      "Req-1--PAXOS_PREPARE--From-1--To-0",
      "Req-1--PAXOS_PREPARE--From-1--To-1",
      "Req-1--PAXOS_PREPARE--From-1--To-2",
      "Req-1--PAXOS_PREPARE_RESPONSE--From-0--To-1",
      "Req-1--PAXOS_PREPARE_RESPONSE--From-1--To-1",
      "Req-1--PAXOS_PREPARE_RESPONSE--From-2--To-1",
      "Req-1--PAXOS_PROPOSE--From-1--To-0",
      "Req-1--PAXOS_PROPOSE--From-1--To-1",
      "Req-1--PAXOS_PROPOSE--From-1--To-2",
      "Req-1--PAXOS_PROPOSE_RESPONSE--From-0--To-1",
      "Req-1--PAXOS_PROPOSE_RESPONSE--From-1--To-1",
      "Req-1--PAXOS_PROPOSE_RESPONSE--From-2--To-1",
      "Req-1--PAXOS_COMMIT--From-1--To-0",
      "Req-1--PAXOS_COMMIT--From-1--To-1",
      "Req-1--PAXOS_COMMIT--From-1--To-2",
      "Req-1--PAXOS_COMMIT_RESPONSE--From-0--To-1",
      "Req-1--PAXOS_COMMIT_RESPONSE--From-1--To-1",
      "Req-1--PAXOS_COMMIT_RESPONSE--From-2--To-1",

      "Req-2--PAXOS_PREPARE--From-2--To-0",
      "Req-2--PAXOS_PREPARE--From-2--To-1",
      "Req-2--PAXOS_PREPARE--From-2--To-2",
      "Req-2--PAXOS_PREPARE_RESPONSE--From-0--To-2",
      "Req-2--PAXOS_PREPARE_RESPONSE--From-1--To-2",
      "Req-2--PAXOS_PREPARE_RESPONSE--From-2--To-2",
      "Req-2--PAXOS_PROPOSE--From-2--To-0",
      "Req-2--PAXOS_PROPOSE--From-2--To-1",
      "Req-2--PAXOS_PROPOSE--From-2--To-2",
      "Req-2--PAXOS_PROPOSE_RESPONSE--From-0--To-2",
      "Req-2--PAXOS_PROPOSE_RESPONSE--From-1--To-2",
      "Req-2--PAXOS_PROPOSE_RESPONSE--From-2--To-2",

      "Req-2--PAXOS_COMMIT--From-2--To-0",
      "Req-2--PAXOS_COMMIT--From-2--To-1",
      "Req-2--PAXOS_COMMIT--From-2--To-2",
      "Req-2--PAXOS_COMMIT_RESPONSE--From-0--To-2",
      "Req-2--PAXOS_COMMIT_RESPONSE--From-1--To-2",
      "Req-2--PAXOS_COMMIT_RESPONSE--From-2--To-2"
  );
}
