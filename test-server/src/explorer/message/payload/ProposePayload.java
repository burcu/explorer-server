package explorer.message.payload;

import com.google.gson.Gson;

public class ProposePayload extends Payload {
  public final String ballot;
  public final String key;

  public ProposePayload(String ballot, String key) {
    this.ballot = ballot;
    this.key = key;
  }

  public static ProposePayload toObject(String json) {
    Gson gson = new Gson();
    //System.out.println(json);
    return gson.fromJson(json, ProposePayload.class);
  }

  @Override
  public String getBallot() {
    return ballot;
  }
}
