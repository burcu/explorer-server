package explorer.message.payload;

import com.google.gson.Gson;

public class CommitPayload extends Payload {
  public final String ballot;
  public final String key;

  public CommitPayload(String ballot, String key) {
    this.ballot = ballot;
    this.key = key;
  }

  public static CommitPayload toObject(String json) {
    Gson gson = new Gson();
    //System.out.println(json);
    return gson.fromJson(json, CommitPayload.class);
  }

  @Override
  public String getBallot() {
    return ballot;
  }

}
