package explorer.message.payload;

import com.google.gson.Gson;

public class ProposeResponsePayload extends Payload {
  public final boolean response;

  public ProposeResponsePayload(boolean response) {
    this.response = response;
  }

  public static ProposeResponsePayload toObject(String json) {
    Gson gson = new Gson();
    //System.out.println(json);
    return gson.fromJson(json, ProposeResponsePayload.class);
  }

  @Override
  public String getBallot() {
    return "";
  }
}
