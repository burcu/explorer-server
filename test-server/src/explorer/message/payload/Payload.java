package explorer.message.payload;

import com.google.gson.Gson;
import explorer.message.PaxosMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Payload {
  private static final Logger log = LoggerFactory.getLogger(Payload.class);

  public static final String ACK_PAYLOAD = "ACK";

  public abstract String getBallot();

  public static Payload createPayload(int protocolStep, String payloadStr) {
    if (ACK_PAYLOAD.equals(payloadStr)) {
      return new AckPayload();
    }
    switch(protocolStep) {
      case 0:
        return PreparePayload.toObject(payloadStr);
      case 1:
        return PrepareResponsePayload.toObject(payloadStr);
      case 2:
        return ProposePayload.toObject(payloadStr);
      case 3:
        return ProposeResponsePayload.toObject(payloadStr);
      case 4:
        return CommitPayload.toObject(payloadStr);
      case 5:
        return CommitResponsePayload.toObject(payloadStr);
      default:
        log.error("Unknown protocol step for payload: " + payloadStr);
        return null;
    }
  }

}
