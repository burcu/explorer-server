package explorer.message.payload;

import com.google.gson.Gson;

public class PrepareResponsePayload extends Payload {
  public final String mostRecentCommitKey;
  public final String inProgressCommitKey;
  public final boolean response;
  public final String inProgressCommitBallot;
  public final String mostRecentCommitBallot;

  public PrepareResponsePayload(String mostRecentCommitKey, String inProgressCommitKey, boolean response, String inProgressCommitBallot, String mostRecentCommitBallot) {
    this.mostRecentCommitKey = mostRecentCommitKey;
    this.inProgressCommitKey = inProgressCommitKey;
    this.response = response;
    this.inProgressCommitBallot = inProgressCommitBallot;
    this.mostRecentCommitBallot = mostRecentCommitBallot;
  }

  public static PrepareResponsePayload toObject(String json) {
    Gson gson = new Gson();
    //System.out.println(json);
    return gson.fromJson(json, PrepareResponsePayload.class);
  }

  @Override
  public String getBallot() {
    return "";
  }

  public String getMostRecentBallot() {
    return mostRecentCommitBallot;
  }

  public String getInProgressBallot() {
    return inProgressCommitBallot;
  }
}
