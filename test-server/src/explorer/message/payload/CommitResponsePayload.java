package explorer.message.payload;

import com.google.gson.Gson;

public class CommitResponsePayload extends Payload {

  @Override
  public String getBallot() {
    return "";
  }

  public static CommitResponsePayload toObject(String json) {
    Gson gson = new Gson();
    //System.out.println(json);
    return gson.fromJson(json, CommitResponsePayload.class);
  }
}
