package explorer.message;

import java.util.Comparator;

public class MessageComparator implements Comparator<PaxosMessage> {

    public int compare(PaxosMessage o1, PaxosMessage o2) {
        String id1 = o1.getMessageContentWithBallot();
        String id2 = o2.getMessageContentWithBallot();

        if(id1.compareTo(id2) < 0) return -1;
        else if (id2.compareTo(id1) > 0) return 1;
        return 0;
    }
}
