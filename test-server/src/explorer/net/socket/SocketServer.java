package explorer.net.socket;

import explorer.net.Handler;
import explorer.net.TestingServer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SocketServer implements TestingServer {

  private static Logger log = LoggerFactory.getLogger(SocketServer.class);

  private ServerSocket serverSocket;
  private int numClients;
  private Handler handler;

  private Map<Integer, SocketConnection> connections = new HashMap<>();
  private List<Thread> threads = new ArrayList<>();

  public SocketServer(int portNumber, int clients, Handler handler) {
    this.handler = handler;
    this.numClients = clients;

    try {
      this.serverSocket = new ServerSocket(portNumber);
    } catch (IOException e) {
      log.error("Could not listen on port: " + portNumber, e);
    }
  }

  @Override
  public void run() {

    boolean shouldRun = true;
    while (shouldRun) {
      try {
        log.info("Waiting for connections from Cassandra nodes...");

        Socket clientSocket = serverSocket.accept();
        int id = connections.size() + 1;

        SocketConnection c = new SocketConnection(clientSocket, id, handler);
        connections.put(id, c);

        Thread thread = new Thread(c, "client-runner-" + id);
        threads.add(thread);
        thread.start();
      } catch (SocketException se) {
        // do nothing, socket closed
        shouldRun = false;
      } catch (IOException e) {
        log.error("Accept failed.", e);
        System.exit(1);
      }

      if(connections.size() == numClients) {
        synchronized (this) {
          for (Thread t : threads) {
            try {
              t.join();
            } catch (InterruptedException e) {
              log.warn("Requested thread exit");
            }
          }
        }
      }
    }
  }

  @Override
  public void stop() {
    for (Thread t : threads) {
      t.interrupt();
    }
    log.info("Closing the server socket.");
    try {
      serverSocket.close();
    } catch (IOException e) {
      // Socket is closed, accept
    }
  }

  @Override
  public void clear() {
    for (Thread t : threads) {
      t.interrupt();
    }
    synchronized (this) {
      threads.clear();
    }
    connections.clear();
  }
}
