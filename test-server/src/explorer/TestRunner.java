package explorer;

import explorer.message.PaxosMessage;
import explorer.net.Handler;
import explorer.net.TestingServer;
import explorer.net.socket.SocketServer;
import explorer.schedule.*;
import explorer.scheduler.*;
import explorer.verifier.CassVerifier;
import explorer.workload.CassWorkloadDriver;
import explorer.workload.WorkloadDriver;
import org.slf4j.LoggerFactory;
import explorer.utils.FileUtils;

import java.util.*;

/**
 * Runs Cassandra cluster with the specified scheduler and test parameters
 *
 * The arguments in the ExplorerConf can be overwritten by providing arguments:
 * e.g. randomSeed=12347265
 */
public class TestRunner {
  private static final org.slf4j.Logger log = LoggerFactory.getLogger(TestRunner.class);
  public final ExplorerConf conf;

  // Server for collecting Cassandra events and managing workload
  private final Thread serverThread;
  private final TestingServer testingServer;
  protected final WorkloadDriver workloadDriver;

  // Schedule explorer, proxy to server connection, scheduler
  protected final SchedulerProxy schedulerProxy;

  protected Scheduler currentScheduler;

  public TestRunner(ExplorerConf conf) {
    this.conf = conf;

    schedulerProxy = new SchedulerProxy();

    // start server which enforces a schedule over distributed system nodes
    Handler handler = new ConnectionHandler(schedulerProxy);
    testingServer = new SocketServer(conf.portNumber, conf.NUM_CLIENTS, handler);
    serverThread = new Thread(testingServer, "testing-server");
    serverThread.start();

    // start workload driver
    workloadDriver = new CassWorkloadDriver(conf.getWorkloadDirs(), conf.NUM_CLIENTS, conf.javaPath);

    // add shutdown hook to stop Cassandra processes when test terminates
    Runtime.getRuntime().addShutdownHook(new Thread(() -> {
      workloadDriver.stopEnsemble();
      sleepFor(1000);
    }));
  }

  public void startTest(int testId, WorkloadDriver workloadDriver, Scheduler scheduler) {
    log.info("Running test: " + testId);
    schedulerProxy.init(scheduler);

    // reset workload and Cassandra cluster
    workloadDriver.cleanup();
    workloadDriver.prepare(testId);
    workloadDriver.startEnsemble();
    sleepFor(4000);
  }

  /**
   * Starts (prepares data and run dirs), runs (executes a schedule), ends (cleans up) a test case
   * @param testId testID
   * @param schedule schedule to execute
   * @return test result - list of executed messages and whether the execution is verified or not (is buggy)
   */
  public TestResult runTestSchedule(int testId, Schedule schedule) {
    logToResultsFile("\nRunning test: " + testId);

    // the maximum number of consequent pollings when no events are received from the Cassandra cluster
    int MAX_NUM_POLLING = 5;
    int numTrials = 1;
    List<PaxosMessage> events = null;
    boolean verified = true;
    while(events == null && numTrials < MAX_NUM_POLLING) {
      // todo add a factory method for scheduler generation based on the type of the schedule
      if(schedule instanceof EventListSchedule)
        currentScheduler = new EventListScheduler((EventListSchedule)schedule, conf.getRandomSeed());
      else if(schedule instanceof PriorityListSchedule)
        currentScheduler = new POSScheduler(schedule);
      else if(schedule instanceof FileSchedule)
        currentScheduler = new FileReplayingScheduler((FileSchedule) schedule);
      else if(schedule instanceof PCTSchedule) {
        currentScheduler = new PCTReplayingScheduler((PCTSchedule) schedule, conf.getRandomSeed());
        logToResultsFile("PCTReplayingScheduler started with seed: " + conf.getRandomSeed());
      } else {
        log.error("Unsupported schedule");
        System.exit(-1);
        return null;
      }

      // start cluster, workload processor, cleanup scheduler proxy
      startTest(testId, workloadDriver, currentScheduler);

      // run a test with schedule cp.getSchedule(testId - 1)
      // if it does not schedules any events (due to connection problems to nodes), repeat the test
      events = runTestSchedule();

      // check property, update coverage, stop cluster
      verified = endTest(workloadDriver, (events != null));
      numTrials ++;
    }

    return new TestResult(events, verified);
  }

  protected List<PaxosMessage> runTestSchedule() {
    int numQueriesSubmitted = 0;
    workloadDriver.submitQuery(0, numQueriesSubmitted++);

    // wait for all initial messages
    boolean testStarted = waitForInitialMessages();
    int numIdle = 0, maxNumIdle = 5;
    boolean testEnded = false;

    while(testStarted && !testEnded && !schedulerProxy.isScheduleCompleted()) {
      schedulerProxy.sendCollectedEvents();
      boolean scheduled = schedulerProxy.scheduleNext();

      if(scheduled) {
        if(numQueriesSubmitted < conf.NUM_REQUESTS && schedulerProxy.numMessagesScheduled() == conf.getWorkloadPoints()[numQueriesSubmitted-1]) {
          workloadDriver.submitQuery(numQueriesSubmitted, numQueriesSubmitted);
          numQueriesSubmitted++;
        }
        numIdle = 0;
      } else {
        numIdle ++;
      }

      // wait until new messages arrive
      sleepFor(500);

      if(numIdle > maxNumIdle)
        testEnded = true;
    }

    return schedulerProxy.getScheduledMessages();
  }

  public List<PaxosMessage> runTestWithScheduler(String schedulerClass, int testId) {
    // the maximum number of consequent pollings when no events are received from the Cassandra cluster
    int MAX_NUM_POLLING = 5;
    int numTrials = 1;
    List<PaxosMessage> events = null;

    while(events == null && numTrials < MAX_NUM_POLLING) {
      conf.setSchedulerClass(schedulerClass);
      currentScheduler = SchedulerFactory.createScheduler(conf, 0); // increments random seed by testId

      // start cluster, workload processor, cleanup scheduler proxy
      logToResultsFile("\nRunning node failure injection test: " + testId);
      startTest(testId, workloadDriver, currentScheduler);

      // run a test with schedule cp.getSchedule(testId - 1)
      // if it does not schedules any events (due to connection problems to nodes), repeat the test
      events = runTestSchedule();

      // check property, update coverage, stop cluster
      endTest(workloadDriver, (events != null));
      numTrials ++;
    }

    return events;
  }

  // returns true if all initial messages are successfully collected
  protected boolean waitForInitialMessages() {
    int numIdle = 0;
    int maxNumIdle = 5;
    while(schedulerProxy.numMessagesReceived() < conf.NUM_PROCESSES) {
      sleepFor(500);
      numIdle++;
      if(numIdle > maxNumIdle) {
        log.error("Cannot connect to the nodes/collect messages");
        return false;
      };
    }
    return true;
  }

  private boolean endTest(WorkloadDriver workloadDriver, boolean testSuccessful) {
    boolean verified = false;
    if(testSuccessful) {
      CassVerifier verifier = new CassVerifier();
      verified = verifier.verify();
      logToResultsFile(currentScheduler.getStats() + verifier.getResultString());
    }

    workloadDriver.stopEnsemble();
    sleepFor(3000);
    testingServer.clear();
    return verified;
  }

  protected void logToResultsFile(String s) {
    FileUtils.writeToFile(conf.getResultFileName(), s, true);
  }

  public void sleepFor(int msec) {
    try {
      Thread.sleep(msec);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void stopTestServer() {
    testingServer.stop();
    serverThread.interrupt();
  }

  protected static class TestResult {
    final List<PaxosMessage> messages;
    final boolean verified;

    TestResult(List<PaxosMessage> messages, boolean verified) {
      this.messages = messages;
      this.verified = verified;
    }

  }
}