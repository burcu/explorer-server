package explorer.mock;

import explorer.verifier.CassVerifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Scanner;

public class MockNode {
    private static final Logger log = LoggerFactory.getLogger(MockNode.class);
    int nodeId;

    public MockNode(int nodeId) {
        this.nodeId = nodeId;

        TestingClient tc = new TestingClient("127.0.0.1", 4444);

        Thread t = new Thread(new Runnable() {

            @Override
            public void run() {
                tc.connect();
            }
        });
        t.start();

        Scanner s = new Scanner(System.in);
        String str = "";

        while(!str.equals("END")) {
            str = s.nextLine();
            tc.writeToSocket();
        }

        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(int msg) {
        log.info("NODE " + nodeId + " has sent the message: " + msg);
    }

    public void receiveMessage(int msg) {
        log.info("NODE " + nodeId + " has received the message: " + msg);
    }

}
