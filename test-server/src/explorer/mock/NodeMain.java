package explorer.mock;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NodeMain {
    private static final Logger log = LoggerFactory.getLogger(NodeMain.class);
    public static void main(String[] args) {
        BasicConfigurator.configure();
        org.apache.log4j.Logger.getRootLogger().setLevel(Level.INFO);
        if (args.length == 1) {
            new MockNode(Integer.parseInt(args[0]));
        } else {
            new MockNode(0);
        }
    }
}
