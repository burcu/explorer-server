package explorer.mock;

import explorer.message.PaxosMessage;
import explorer.message.payload.PreparePayload;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class TestingClient {

  private final String hostName;
  private final int portNumber;

  private PrintWriter out;
  private BufferedReader in;

  public TestingClient(String hostName, int portNumber) {
    this.hostName = hostName;
    this.portNumber = portNumber;
  }

  public void writeToSocket() {
    out.println(PaxosMessage.toJsonStr(new PaxosMessage(1, 1, 2, "PAXOS_PREPARE", "{\"ballot\"=\"33da8d30-08c5-11e7-845e-\", \"key\"=\"1916154799\"}", "1")));
  }

  public void connect() {
    Socket socket;
    try {
      socket = new Socket(hostName, portNumber);
      out = new PrintWriter(socket.getOutputStream(), true);
      in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      runInLoop();
    } catch (UnknownHostException e) {
      System.err.println("Don't know about host " + hostName);
      System.exit(1);
    } catch (IOException e) {
      System.err.println("Couldn't get I/O for the connection to " + hostName);
      System.exit(1);
    }
  }

  public void disconnect() {

  }

  public void runInLoop() {
    String fromServer;

    try {
      while ((fromServer = in.readLine()) != null) {
        if (fromServer.equals("END"))
          break;
      }
    } catch (Exception e) {
      System.err.println(e.getMessage());
      System.exit(1);
    }
  }
}