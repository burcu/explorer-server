package explorer.fuzz;

import explorer.fuzz.fitness.Fitness;
import explorer.schedule.PriorityListSchedule;
import explorer.schedule.Schedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * GA based (or Search based fuzzing based) Schedule Explorer
 * Given a Fitness calculation (e.g. using out-of-order messages or state divergence between replicas)
 *    - Mutate fittest individuals(schedules) to generate and test a new population
 */
public class GAPopulation {
  private static final Logger log = LoggerFactory.getLogger(GAPopulation.class);

  Map<Schedule, Fitness> exploredSchedules;
  List<Schedule> currPopulation;

  private final Random random;
  private final int randomSeed;
  private final int populationSize;
  private final int elitesSize;
  private final int individualLength;

  public GAPopulation(int randomSeed, int populationCount, int elitesSize, int individualLength) {
    exploredSchedules = new HashMap<>();
    this.randomSeed = randomSeed;
    random = new Random(randomSeed);
    this.populationSize = populationCount;
    this.elitesSize = elitesSize;
    this.individualLength = individualLength;

    currPopulation = randomPopulation();
  }

  public List<Schedule> randomPopulation() {
    List<Schedule> newPopulation = new ArrayList<>();
    // initial priorities are randomly selected
    for(int i = 0; i < populationSize; i++) {
      Schedule schedule = new PriorityListSchedule(random, individualLength);
      newPopulation.add(schedule);
    }
    return newPopulation;
  }

  // create a population around the given schedule
  public void updatePopulation(Schedule initial) {
    currPopulation.clear();

    for(int i = 0; i < populationSize/2; i++) {
      Schedule s = initial.mutateRandomEvent(random);
      currPopulation.add(s);
    }

    for(int i = 0; i < populationSize/2; i++) {
      Schedule s = initial.mutateRandomEventPair(random);
      currPopulation.add(s);
    }
  }

  public void updatePopulation(boolean keepElites) {
    List<Schedule> oldPopulation = new ArrayList<>(currPopulation);
    currPopulation.clear();

    // take the fittest individuals of the population
    sortPopulation(oldPopulation);

    if(!keepElites) {
      // add mutations of each
      for(int i = 0; i < elitesSize; i++) {
        for(int j = 0; j < populationSize / elitesSize; j++) {
          Schedule s = oldPopulation.get(oldPopulation.size() - 1 - i).mutateRandomEvent(random);
          currPopulation.add(s);
        }
      }
    /*} else {
      for(int i = 0; i < elitesSize; i++) {
        currPopulation.add(oldPopulation.get(oldPopulation.size() - i - 1));
        if((populationSize / elitesSize) % 2 == 0) currPopulation.add(oldPopulation.get(oldPopulation.size() - i - 1));
        for(int j = 0; j < (populationSize / elitesSize) / 2; j++) {
          Schedule s = oldPopulation.get(oldPopulation.size() - 1 - i).mutateRandomEvent(random).mutateRandomEvent(random); //.mutateRandomEvent(random);
          currPopulation.add(s);
        }
        for(int j = 0; j < (populationSize / elitesSize) / 2; j++) {
          Schedule s = oldPopulation.get(oldPopulation.size() - 1 - i).mutateRandomEventPair(random).mutateRandomEventPair(random); //.mutateRandomEventPair(random);
          currPopulation.add(s);
        }
      }
    }*/
    } else {
      for(int i = 0; i < elitesSize; i++) {
        currPopulation.add(oldPopulation.get(oldPopulation.size() - i - 1));
        //if((populationSize / elitesSize) % 2 == 0) currPopulation.add(oldPopulation.get(oldPopulation.size() - i - 1));
        for(int j = 0; j < (populationSize / elitesSize)/4; j++) {
          Schedule s1 = oldPopulation.get(oldPopulation.size() - 1 - i).mutateRandomEventPair(random);
          //Schedule s2 = oldPopulation.get(oldPopulation.size() - 1 - i).mutateRandomNewComer(random);
          Schedule s2 = oldPopulation.get(oldPopulation.size() - 1 - i).dropRandomEvent(random);
          currPopulation.add(s1);
          currPopulation.add(s2);
          Schedule s3 = oldPopulation.get(oldPopulation.size() - 1 - i);//.dropRandom000(random);
          Schedule s4 = oldPopulation.get(oldPopulation.size() - 1 - i);//.mutateRandomOOO(random);
          currPopulation.add(s3);
          currPopulation.add(s4);
        }
      }
    }

  }
  
  private void sortPopulation(List<Schedule> schedules) {
    schedules.sort(new Comparator<Schedule>() {
      @Override
      public int compare(Schedule o1, Schedule o2) {
        if(exploredSchedules.get(o1) == null || exploredSchedules.get(o1) == null) {
          log.error("Comparing schedules with no set fitness!");
          System.exit(-1);
          return 0;
        }
        return exploredSchedules.get(o1).compareTo(exploredSchedules.get(o2));
      }
    });
  }

  public void addExplored(Schedule schedule, Fitness fitness) {
    exploredSchedules.put(schedule, fitness);
  }

  public Fitness getFitness(Schedule schedule) {
    return exploredSchedules.get(schedule);
  }

  public Fitness getFitness(int index) {
    return exploredSchedules.get(getSchedule(index));
  }

  public Schedule getSchedule(int index) {
    if(index >= currPopulation.size() ) {
      log.error("Population does not have schedule indexed: " + index);
      //System.exit(-1);
    }
    return currPopulation.get(index);
  }

  public void setSchedule(int index, Schedule schedule) {
    if(index >= currPopulation.size() ) {
      log.error("Population does not have schedule indexed: " + index);
      //System.exit(-1);
    }
    currPopulation.set(index, schedule);
  }

  public String getConfigAsStr() {
    StringBuilder sb = new StringBuilder("Schedule explorer with:");
    sb.append("\nRandom seed: ").append(randomSeed);
    sb.append("\nPopulation size: ").append(populationSize);
    return sb.toString();
  }

}
