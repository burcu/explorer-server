package explorer.fuzz.fitness;

import explorer.message.PaxosMessage;

import java.util.*;

public abstract class OOODistanceFitness extends Fitness {

    protected final int[] numOutOfOrder;
    protected final int[] distance;

    protected final int totalOutOfOrder;
    protected final int totalDistance;
    public final int numCommitted;

    protected Map<String, Integer> commitBallots; // number of times ballots are in-order-committed
    protected List<String> ballots; // number of times ballots are in-order-committed

    private final static int NUM_ROUNDS = 3;

    public OOODistanceFitness(List<PaxosMessage> scheduledMessages, List<PaxosMessage> toScheduleMessages, int numProcesses) {
        super(scheduledMessages, toScheduleMessages, numProcesses);

        numOutOfOrder = new int[numProcesses];
        distance = new int[numProcesses];
        PaxosMessage[] mostRecent = new PaxosMessage[numProcesses];
        commitBallots = new TreeMap<>();
        ballots = new ArrayList<>();

        for(PaxosMessage m: scheduledMessages) {
            if(!ballots.contains(m.getBallot()))
                ballots.add(m.getBallot());
        }
        //Collections.sort(ballots);
        ballots.sort(new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                if (Integer.parseInt(o1.substring(0, 6), 16) < Integer.parseInt(o2.substring(0, 6), 16)) return -1;
                if (Integer.parseInt(o1.substring(0, 6), 16) == Integer.parseInt(o2.substring(0, 6), 16)) return 0;
                return 1;
            }
        });

        //System.out.println("Ballots: ");
        //for(String b: ballots) System.out.print(b + "   ");

        for(PaxosMessage m: scheduledMessages) {
            if(mostRecent[(int)m.getRecv()] == null)
                mostRecent[(int)m.getRecv()] = m;

            if(m.hasSmallerBallot(mostRecent[(int)m.getRecv()])) {
                // check ballots:
                distance[(int)m.getRecv()] += (ballots.indexOf(mostRecent[(int)m.getRecv()].getBallot()) - ballots.indexOf(m.getBallot())) * NUM_ROUNDS;
                numOutOfOrder[(int)m.getRecv()] ++;
            } else if(m.getBallot().equals(mostRecent[(int)m.getRecv()].getBallot()) && m.getProtocolStep() < mostRecent[(int)m.getRecv()].getProtocolStep()) {
                // check rounds
                distance[(int)m.getRecv()] += (mostRecent[(int)m.getRecv()].getProtocolStep() - m.getProtocolStep());
                numOutOfOrder[(int)m.getRecv()] ++;

            } else {
                mostRecent[(int)m.getRecv()] = m;

                // Add a commit message's COMMIT only if it's not out of order
                if(m.getVerb().equals("PAXOS_COMMIT")) {
                    if(commitBallots.containsKey(m.getPayloadBallot()))
                        commitBallots.put(m.getPayloadBallot(), commitBallots.get(m.getPayloadBallot()) + 1);
                    else
                        commitBallots.put(m.getPayloadBallot(), 1);
                }
            }
        }

        int committedByMajority = 0;
        for(String b: commitBallots.keySet())
            if(commitBallots.get(b) >= (numProcesses / 2 + 1))
                committedByMajority ++;

        numCommitted = committedByMajority;

        this.totalOutOfOrder = sumOutOfOrder();
        this.totalDistance = sumOfDistance();
    }


    private int sumOutOfOrder() {
        int total = 0;
        for (int value : numOutOfOrder) {
            total += value;
        }
        return total;
    }

    private int sumOfDistance() {
        int total = 0;
        for (int value : distance) {
            total += value;
        }
        return total;
    }

    public String toString() {
        StringBuilder ballots = new StringBuilder("");
        for(String s: commitBallots.keySet())
            ballots.append(s).append(",");

        String resultStr = "#Out-of-order: " + numOutOfOrder[0] + "," + numOutOfOrder[1]  + ","
                + numOutOfOrder[2] + " #Total: " + totalOutOfOrder //+ "\nBallots: " + ballots.toString();
                +"\nDistance: "+ distance[0] + "," + distance[1]  + ","
                + distance[2] +  "\n#TotalDistance: " + sumOfDistance();

        return resultStr.concat("\n");
    }
}
