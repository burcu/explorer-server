package explorer.fuzz.fitness;

import explorer.message.PaxosMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class OOOCountFitness extends Fitness {

  protected List<PaxosMessage> oooMessages = new ArrayList<>();
  protected final int[] numOutOfOrder;
  protected final int totalOutOfOrder;

  protected Map<String, Integer> commitBallots; // number of times ballots are in-order-committed

  public OOOCountFitness(List<PaxosMessage> scheduledMessages, List<PaxosMessage> toScheduleMessages, int numProcesses) {
    super(scheduledMessages, toScheduleMessages, numProcesses);

    numOutOfOrder = new int[numProcesses];
    PaxosMessage[] mostRecent = new PaxosMessage[numProcesses];
    commitBallots = new TreeMap<>();

    for(PaxosMessage m: scheduledMessages) {
      if(mostRecent[(int)m.getRecv()] == null)
        mostRecent[(int)m.getRecv()] = m;

      if(m.hasSmallerBallot(mostRecent[(int)m.getRecv()])
              || (m.getBallot().equals(mostRecent[(int)m.getRecv()].getBallot()) && m.getProtocolStep() < mostRecent[(int)m.getRecv()].getProtocolStep()) ) {
        numOutOfOrder[(int)m.getRecv()] ++;
        oooMessages.add(m);

      } else {
        mostRecent[(int)m.getRecv()] = m;

        // Add a commit message's COMMIT only if it's not out of order
        if(m.getVerb().equals("PAXOS_COMMIT")) {
          if(commitBallots.containsKey(m.getPayloadBallot()))
            commitBallots.put(m.getPayloadBallot(), commitBallots.get(m.getPayloadBallot()) + 1);
          else
            commitBallots.put(m.getPayloadBallot(), 1);
        }
      }
    }

    this.totalOutOfOrder = sumOutOfOrder();
  }

  @Override
  public int compareTo(Fitness f) {
    if(!(f instanceof OOOCountFitness)) return 0;

    DivergenceFitness other = (DivergenceFitness)f;
    if (totalOutOfOrder > other.totalOutOfOrder) return -1; // the less out of order, the better
    if (totalOutOfOrder == other.totalOutOfOrder) return 0;
    return 1;
  }

  private int sumOutOfOrder() {
    int total = 0;
    for (int value : numOutOfOrder) {
      total += value;
    }
    return total;
  }

  public List<PaxosMessage> getOooMessages() {
    return oooMessages;
  }

  //todo revise!
  public List<PaxosMessage> getAllOOOMessages() {
    List<PaxosMessage> ooo = new ArrayList<>();
    PaxosMessage mostRecent = scheduledMessages.get(0);
    for(PaxosMessage m: scheduledMessages) {
      if (m.getClientRequest() < mostRecent.getClientRequest() || (m.getClientRequest() == mostRecent.getClientRequest()) && m.getProtocolStep() < mostRecent.getProtocolStep()) {
        ooo.add(m);
      } else {
        mostRecent = m;
      }
    }
    return ooo;
  }

  public String toString() {
    StringBuilder ballots = new StringBuilder("");
    for(String s: commitBallots.keySet())
      ballots.append(s).append(",");

    String resultStr = "#Out-of-order: " + numOutOfOrder[0] + "," + numOutOfOrder[1]  + ","
        + numOutOfOrder[2] + " #Total: " + totalOutOfOrder + "\nBallots: " + ballots.toString();

    return resultStr.concat("\n");
  }

}
