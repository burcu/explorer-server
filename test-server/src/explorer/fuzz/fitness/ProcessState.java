package explorer.fuzz.fitness;

public class ProcessState {
    private final String mostRecentCommit;
    private final String inProgressCommit;

    public ProcessState(String mostRecentCommit, String inProgressCommit) {
        this.mostRecentCommit = mostRecentCommit;
        this.inProgressCommit = inProgressCommit;
    }

    public String getMostRecentCommit() {
        return mostRecentCommit;
    }

    public String getInProgressCommit() {
        return inProgressCommit;
    }

    public String toString() {
        return "MostRecentCommit: " + mostRecentCommit + " InProgressCommit: " + inProgressCommit + "\n";
    }
}
