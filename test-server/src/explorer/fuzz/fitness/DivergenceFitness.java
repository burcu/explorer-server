package explorer.fuzz.fitness;

import explorer.message.PaxosMessage;
import explorer.message.payload.PrepareResponsePayload;

import java.util.*;


/**
 * Keeps divergence of replicas using their answers to transaction requests
 */
public class DivergenceFitness extends OOOCountFitness {

  // for each process, a map from ballot to process state
  // (the state of the replica when it receives a request with a ballot)
  List<Map<String, ProcessState>> processStates;

  // number of divergent states for each ballot
  Map<String, Integer> divergentStates;

  private final int numDivergentPairs;

  public DivergenceFitness(List<PaxosMessage> scheduledMessages, List<PaxosMessage> toScheduleMessages, int numProcesses) {
    super(scheduledMessages, toScheduleMessages, numProcesses);

    processStates = new ArrayList<>();
    for(int i = 0; i < numProcesses; i++) {
      processStates.add(new HashMap<>());
    }

    Set<String> ballotsWithStates = new HashSet<>(); // ballots for which states are recorded
    // get process states from messages with states
    for(PaxosMessage m: scheduledMessages) {
      //System.out.println(m.getMessageContentWithSeqNumber());
      if(m.getProtocolStep() == 1 ) {
        int process = (int)m.getSender();
        String currentBallot = m.getBallot();
        ballotsWithStates.add(currentBallot);
        ProcessState ps = new ProcessState(((PrepareResponsePayload) m.getPayloadObj()).getMostRecentBallot(), ((PrepareResponsePayload) m.getPayloadObj()).getInProgressBallot());
        processStates.get(process).put(currentBallot, ps);
      }
    }
    // get process states from on-flight messages
    for(PaxosMessage m: toScheduleMessages) {
      //System.out.println(m.getMessageContentWithSeqNumber());
      if(m.getProtocolStep() == 1 && ballotsWithStates.contains(m.getBallot())) {
        int process = (int)m.getSender();
        String currentBallot = m.getBallot();
        ProcessState ps = new ProcessState(((PrepareResponsePayload) m.getPayloadObj()).getMostRecentBallot(), ((PrepareResponsePayload) m.getPayloadObj()).getInProgressBallot());
        processStates.get(process).put(currentBallot, ps);
      }
    }

    divergentStates = new HashMap<String, Integer>();

    for(String b: commitBallots.keySet()) {
      divergentStates.put(b, 0);
      for(int i = 0; i < numProcesses; i++) {
        for(int j = i + 1; j < numProcesses; j++) {
          if(processStates.get(i).get(b) != null && processStates.get(j).get(b) != null) {
            String inProgress1 = processStates.get(i).get(b).getInProgressCommit();
            String inProgress2 = processStates.get(j).get(b).getInProgressCommit();
            String mostRecent1 = processStates.get(i).get(b).getMostRecentCommit();
            String mostRecent2 = processStates.get(j).get(b).getMostRecentCommit();
            if(!inProgress1.equals(inProgress2) || !mostRecent1.equals(mostRecent2)) {
              divergentStates.put(b, divergentStates.get(b) + 1);
              //System.out.println("\nDivergent at ballot: " + b);
              //System.out.println("Processes: " + i + " and " + j);
              //System.out.println("States: " + processStates.get(i).get(b) + " and " + processStates.get(j).get(b));
            }
          } else {
            divergentStates.put(b, divergentStates.get(b) + 1);
          }
        }
      }
    }

    // set the number of divergent pairs of processes
    int count = 0;
    for(String ballot: divergentStates.keySet()) {
      count += divergentStates.get(ballot);
    }
    numDivergentPairs =  count;
  }


  public String toString() {
    String s = "#DivergentPairs: " + numDivergentPairs;
    return super.toString().concat(s).concat("\n");
  }

  public int[] getNumOutOfOrder() {
    return numOutOfOrder;
  }


  @Override
  public int compareTo(Fitness f) {
    if(!(f instanceof DivergenceFitness)) return 0;

    DivergenceFitness other = (DivergenceFitness)f;
    if (numDivergentPairs < other.numDivergentPairs) return -1;
    if (numDivergentPairs == other.numDivergentPairs) {
      if (totalOutOfOrder > other.totalOutOfOrder) return -1; // the less out of order, the better
      if (totalOutOfOrder == other.totalOutOfOrder) return 0;
    }
    return 1;
  }
}
