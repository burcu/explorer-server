package explorer.fuzz.fitness;

import explorer.message.PaxosMessage;

import java.util.List;

/**
 * Evaluates fitness of a schedule based on:
 * - whether it reproduces a bug
 * - how small out-of-order messages it schedules
 */
public class MinimalBuggyFitness extends OOODistanceFitness {

    private final boolean reproducesBug;

    public MinimalBuggyFitness(List<PaxosMessage> scheduledMessages, List<PaxosMessage> toScheduleMessages, int numProcesses, boolean reproducesBug) {
        super(scheduledMessages, toScheduleMessages, numProcesses);
        this.reproducesBug = reproducesBug;
    }

    @Override
    public int compareTo(Fitness f) {
        if(!(f instanceof MinimalBuggyFitness)) return 0;

        MinimalBuggyFitness other = (MinimalBuggyFitness) f;
        if (reproducesBug && !other.reproducesBug) return 1;
        if (!reproducesBug && other.reproducesBug) return -1;

        if (totalOutOfOrder > other.totalOutOfOrder) return -1; // the less out of order, the better
        if (totalOutOfOrder < other.totalOutOfOrder) return 1;

        if(totalDistance > other.totalDistance) return -1;
        if(totalDistance == other.totalDistance) return 0;

        return 1;
    }
}
