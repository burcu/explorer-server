package explorer.fuzz.fitness;

import explorer.message.PaxosMessage;

import java.util.ArrayList;
import java.util.List;

public abstract class Fitness {

    protected List<PaxosMessage> scheduledMessages; // scheduled messages
    protected List<PaxosMessage> toScheduleMessages; // messages on-flight but not scheduled

    public static final Fitness EMPTY_SCHEDULE_FITNESS = new Fitness(new ArrayList<>(), new ArrayList<>(), 0) {
        @Override
        public int compareTo(Fitness f) {
            return -1;
        }
    };

    public Fitness(List<PaxosMessage> scheduledMessages, List<PaxosMessage> toScheduleMessages, int numProcesses) {
        this.scheduledMessages = new ArrayList<>(scheduledMessages);
        this.toScheduleMessages = new ArrayList<>(toScheduleMessages);
    }

    public abstract int compareTo(Fitness f);
}

