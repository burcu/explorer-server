package explorer.fuzz;

import explorer.message.PaxosMessage;
import explorer.schedule.PCTSchedule;
import explorer.schedule.Schedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Coverage-guided Fuzzing based Schedule Explorer
 * (Currently supported for PCT Schedules)
 * Start with an initial population of individuals ((d-1)-tuples to strongly hit)
 * Mutate around schedules that cover unexplored states
 */
public class FuzzPool {
    private static final Logger log = LoggerFactory.getLogger(FuzzPool.class);

    enum STATE_TYPE {MESSAGE_ORDER, MESSAGE_SET, OOO_SET};
    private STATE_TYPE stateType = STATE_TYPE.OOO_SET; // TODO read from config file!

    private List<PCTSchedule> schedulePool; // current list of schedules to be explored
    private final Set<PCTSchedule> exploredSchedules;
    private final Set<State> exploredStates; // A state is defined by message orderings per replicas
    // TODO: Alternative notions of state:
    // - Set of messages (possibly with some content: payload, reply, etc)
    // - OOO sets of each receiver
    // - Higher level abstraction

    private final Random random;
    private final int randomSeed; // kept for logging purposes
    private final int initialPoolSize;

    private final int maxMessages;
    private final int depth;

    public FuzzPool(int randomSeed, int initialPoolSize, int maxMessages, int depth) {
        this.exploredSchedules = new HashSet<>();
        this.exploredStates = new HashSet<>();
        this.randomSeed = randomSeed;
        this.random = new Random(randomSeed);
        this.initialPoolSize = initialPoolSize;
        this.maxMessages = maxMessages;
        this.depth = depth;
        this.schedulePool = new ArrayList<>();
    }

    public List<PCTSchedule> randomPopulation() {
        // generate and run a bunch of PCTSchedules
        List<PCTSchedule> pool = new ArrayList<>();
        // initial priorities are randomly selected
        for (int i = 0; i < initialPoolSize; i++) {
            // Create random tuples and empty chain insertion positions (will be sampled randomly)
            List<Integer> prChangePts = new ArrayList<>();
            for (int j = 0; j < depth - 1; j++) {
                int toAdd = random.nextInt(maxMessages);
                while (prChangePts.contains(toAdd))
                    toAdd = random.nextInt(maxMessages);
                prChangePts.add(toAdd);
            }
            // PCTSchedule: Selected positions for inserting chains to priorities is initially empty
            pool.add(new PCTSchedule(new ArrayList<>(), prChangePts));
        }
        return pool;
    }

    public void setSchedulePool(List<PCTSchedule> schedules) {
        schedulePool = new ArrayList<>(schedules);
    }

    /**
     * Mutates the given list of schedules
     * And sets the generated schedules as the new pool
     *
     * @param schedules schedules to be mutated
     */
    public void updatePool(List<PCTSchedule> schedules) {
        schedulePool.clear();
        for (PCTSchedule schedule : schedules) {
            for(int i = 0; i < 3; i++) { // 3 mutations per schedule
                PCTSchedule mutated = (PCTSchedule) schedule.mutateRandomEvent(random);  //TODO more # of mutated schedules?
                boolean isExplored = isExplored(mutated);
                while (isExplored) {
                    mutated = (PCTSchedule) schedule.mutateRandomEvent(random);
                    isExplored = isExplored(mutated);
                }
                schedulePool.add(mutated);
            }
        }
    }

    public void addExploredSchedule(PCTSchedule schedule) {
        exploredSchedules.add(schedule);
    }

    /**
     * Adds a state into the set of explored states
     *
     * @param messages scheduled in the execution
     * @return true if the state has been explored before
     */
    public boolean checkAndAddState(List<PaxosMessage> messages) {
        if(messages.isEmpty()) return false;

        State newState = null;
        Map<Long, List<PaxosMessage>> messagesByRecv = messages.stream().collect(Collectors.groupingBy(PaxosMessage::getRecv));

        switch (stateType) {
            case MESSAGE_ORDER: {
                // filter messages per node
                List<PaxosMessage> messages1 = messagesByRecv.get(0L);
                List<PaxosMessage> messages2 = messagesByRecv.get(1L);
                List<PaxosMessage> messages3 = messagesByRecv.get(2L);
                newState = new MessageOrderState(messages1, messages2, messages3);
                break;
            }
            case MESSAGE_SET: {
                // filter messages per node
                Set<PaxosMessage> messages1 = new HashSet<>(messagesByRecv.get(0L));
                Set<PaxosMessage> messages2 = new HashSet<>(messagesByRecv.get(1L));
                Set<PaxosMessage> messages3 = new HashSet<>(messagesByRecv.get(2L));
                newState = new MessageSetState(messages1, messages2, messages3);
                break;
            }
            case OOO_SET: {
                Map<Long, List<PaxosMessage>> oooByRecv = Schedule.getAllOOOMessages(messages).stream().collect(Collectors.groupingBy(PaxosMessage::getRecv));
                // filter messages per node
                Set<PaxosMessage> messages1 = new HashSet<>(oooByRecv.get(0L));
                Set<PaxosMessage> messages2 = new HashSet<>(oooByRecv.get(1L));
                Set<PaxosMessage> messages3 = new HashSet<>(oooByRecv.get(2L));
                newState = new MessageSetState(messages1, messages2, messages3);
                break;
            }
            default:
                log.error("Unsupported state type in FuzzPool");
                System.exit(-1);
        }

        for (State s : exploredStates) {
            if (s.isEquivalent(newState))
                return true;
        }

        // add state
        exploredStates.add(newState);
        return false;
    }

    public boolean isExplored(PCTSchedule pctSchedule) {
        for (PCTSchedule s : exploredSchedules) {
            if (s.getPriorityChainPositions().equals(pctSchedule.getPriorityChainPositions()) &&
                    s.getPriorityChangePoints().equals(pctSchedule.getPriorityChangePoints()))
                return true;

        }
        return false;
    }

    public List<PCTSchedule> getSchedulePool() {
        return schedulePool;
    }

    public int getNumExploredSchedules() {
        return exploredSchedules.size();
    }

    public int getNumExploredStates() {
        return exploredStates.size();
    }

    public String getConfigAsStr() {
        return "Fuzz schedule explorer with:" + "\nRandom seed: " + randomSeed +
                "\nInitial pool size: " + initialPoolSize;
    }

    public static abstract class State {
        public abstract boolean isEquivalent(State another);
    }

    public static class MessageOrderState extends State {
        List<PaxosMessage> messages1;
        List<PaxosMessage> messages2;
        List<PaxosMessage> messages3;

        MessageOrderState(List<PaxosMessage> recBy1, List<PaxosMessage> recBy2, List<PaxosMessage> recBy3) {
            this.messages1 = recBy1;
            this.messages2 = recBy2;
            this.messages3 = recBy3;
        }

        @Override
        public boolean isEquivalent(State other) {
            if (!(other instanceof MessageOrderState)) return false;
            MessageOrderState o = (MessageOrderState) other;
            return messages1.equals(o.messages1) && messages2.equals(o.messages2) && messages3.equals(o.messages3);
        }
    }

    public static class MessageSetState extends State {
        Set<PaxosMessage> messages1;
        Set<PaxosMessage> messages2;
        Set<PaxosMessage> messages3;

        MessageSetState(Set<PaxosMessage> recBy1, Set<PaxosMessage> recBy2, Set<PaxosMessage> recBy3) {
            this.messages1 = recBy1;
            this.messages2 = recBy2;
            this.messages3 = recBy3;
        }

        @Override
        public boolean isEquivalent(State other) {
            if (!(other instanceof MessageSetState)) return false;
            MessageSetState o = (MessageSetState) other;
            return messages1.equals(o.messages1) && messages2.equals(o.messages2) && messages3.equals(o.messages3);
        }
    }

    public static class OOOSetState extends State {
        Set<PaxosMessage> messages1;
        Set<PaxosMessage> messages2;
        Set<PaxosMessage> messages3;

        OOOSetState(Set<PaxosMessage> recBy1, Set<PaxosMessage> recBy2, Set<PaxosMessage> recBy3) {
            this.messages1 = recBy1;
            this.messages2 = recBy2;
            this.messages3 = recBy3;
        }

        @Override
        public boolean isEquivalent(State other) {
            if (!(other instanceof OOOSetState)) return false;
            OOOSetState o = (OOOSetState) other;
            return messages1.equals(o.messages1) && messages2.equals(o.messages2) && messages3.equals(o.messages3);
        }
    }
}
