package explorer;

import explorer.fuzz.FuzzPool;
import explorer.message.PaxosMessage;
import explorer.schedule.PCTSchedule;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;


/**
 * Extends TestRunner with a ScheduleExplorer
 *    that performs randomized local search around a given schedule
 *    selecting best schedules among a population of randomly mutated schedules (using MinimalBuggyFitness)
 */
public class FuzzSearchRunner extends TestRunner {
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(RandomLocalSearchRunner.class);

    private final FuzzPool fuzzPool;

    public FuzzSearchRunner(ExplorerConf conf) {
        super(conf);
        fuzzPool = new FuzzPool(conf.getRandomSeed(), conf.populationSize, conf.NUM_MAX_MESSAGES, conf.bugDepth);
        logToResultsFile(fuzzPool.getConfigAsStr());
    }

    /**
     * Explores around the schedule for given number of iterations, with given number of population
     */
    public void fuzzSearch() {
        fuzzPool.setSchedulePool(fuzzPool.randomPopulation());

        int testId = 1, numIterations = 1;
        while(!fuzzPool.getSchedulePool().isEmpty() && numIterations <= conf.numIterations) {
            conf.setRandomSeed(conf.getRandomSeed() + 1); // run schedules and mutators with next seed
            log.info("Running iteration " + testId);

            List<PCTSchedule> pool = fuzzPool.getSchedulePool();
            List<PCTSchedule> newPool = new ArrayList<>();
            for(PCTSchedule schedule: pool) {
                log.info("Running schedule " + schedule);
                log.info("   with chain positions " + schedule.getPriorityChainPositions());

                List<PaxosMessage> messages = runTestSchedule(testId++, schedule).messages;

                // update the population of schedules to be explored
                fuzzPool.addExploredSchedule(schedule);
                boolean coveredBefore = fuzzPool.checkAndAddState(messages);
                if(coveredBefore) logToResultsFile("CCovered before..");
                else logToResultsFile("Not covered before..");

                if(!coveredBefore) newPool.add(schedule);
            }

            // update pool with mutations of schedules in "newPool"
            fuzzPool.updatePool(newPool);
        }

        if(fuzzPool.getSchedulePool().isEmpty()) logToResultsFile("Empty pool");

        System.out.println("Total explored schedules: " + fuzzPool.getNumExploredSchedules());
        System.out.println("Total explored states: " + fuzzPool.getNumExploredStates());
    }

}
