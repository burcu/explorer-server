package explorer.verifier;

import java.util.Map;
import java.util.HashMap;
import com.datastax.driver.core.*;
import explorer.ExplorerConf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CassVerifier  {
  private static final Logger log = LoggerFactory.getLogger(CassVerifier.class);
  private Map<String, String> map = new HashMap<String, String>();

  private String value_1;
  private String value_2;
  private String value_3;

  private String resultString = "";

  public boolean verify() {
    return checkDataConsistency();
  }

  private boolean checkDataConsistency() {
	  getValues();

    try {
      value_1 = map.get("value_1");
      value_2 = map.get("value_2");
      value_3 = map.get("value_3");

      resultString += "Value1: " + value_1 + "    Value2: " + value_2  + "    Value3: " + value_3;// + "\n";

      if (value_1.equals("A") && (value_2.equals("B"))) {
        log.info("\nReproduced the bug.");
        resultString += "\nReproduced the bug." + "\n";
        return false;
      }
    } catch (Exception e) {
      resultString += "Failed to check data consistency." + "\n";
      log.error("Failed to check data consistency.");
      //log.error(e.getMessage());
      return true;
    }
    return true;
  }

  private void getValues(){
		Cluster cluster = Cluster.builder()
        .addContactPoint("127.0.0.1")
        .build();

    Session session = null;
		try {
      session = cluster.connect("test");
			ResultSet rs = session.execute("SELECT * FROM tests");
			Row row = rs.one();
            map.put("owner", row.getString("owner"));
            map.put("value_1", row.getString("value_1"));
            map.put("value_2", row.getString("value_2"));
            map.put("value_3", row.getString("value_3"));
		} catch (Exception e) {
      resultString += "ERROR in reading row." + "\n";
      log.error("ERROR in reading row.");
			//log.error(e.getMessage());
		} finally {
		  if(session != null) session.close();
		  cluster.close();
    }
  }

  public String getResultString() {
    return resultString;
  }

}
