package explorer;

import explorer.utils.FileUtils;
import explorer.workload.WorkloadDirs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class ExplorerConf {

  private static final Logger log = LoggerFactory.getLogger(ExplorerConf.class);

  private static ExplorerConf INSTANCE;

  // parameters of the sampling algorithm
  public final int NUM_PROCESSES;
  public final int NUM_MAX_ROUNDS;
  public final int NUM_MAX_MESSAGES;
  public final int NUM_ROUNDS_IN_PROTOCOL;
  public final int NUM_PHASES;
  public final int NUM_REQUESTS;

  private int[] workloadPoints;

  private int randomSeed;
  public final int bugDepth;
  public final int linkEstablishmentPeriod;

  // used only for naive random sampler
  public final double dropWithProb;

  public final int portNumber;
  public final int NUM_CLIENTS;

  public final int populationSize;
  public final int elitesSize;
  public final int numIterations;

  private String schedulerClass;
  public final String schedulerFile;
  public final boolean schedulerFileHasMsgContent;

  public final String libsDirectory;
  public final String initialDataDirectory;
  public final String runDirectory;
  public final String javaPath;

  // Workload configuration parameters
  public final int clusterPort;
  public final int poolTimeoutMillis;
  public final int readTimeoutMillis;
  public final int timeBetweenQueriesMillis;

  private int numAttempts;

  // Logging configurations
  public final boolean logResult;
  private final String resultFolderName;
  private String resultFileName;
  public boolean logSchedule;

  public final int maxExecutionDuration;

  private ExplorerConf(String configFile, String[] args) {
    Properties prop = loadProperties(configFile);
    Map<String, String> overrideArgs = new HashMap<>();

    if(args != null && args.length != 0) {
      overrideArgs = Arrays.stream(args)
              .filter(s -> s.contains("="))
              .map(s -> Arrays.asList(s.split("=")))
              .collect(Collectors.toMap(kv -> kv.get(0), kv -> kv.get(1)));
    }

    randomSeed =  Integer.parseInt(overrideArgs.getOrDefault("randomSeed", prop.getProperty("randomSeed")));
    bugDepth =  Integer.parseInt(overrideArgs.getOrDefault("bugDepth", prop.getProperty("bugDepth")));
    linkEstablishmentPeriod =  Integer.parseInt(overrideArgs.getOrDefault("linkEstablishmentPeriod", prop.getProperty("linkEstablishmentPeriod")));

    dropWithProb = Double.parseDouble(overrideArgs.getOrDefault("dropWithProb", prop.getProperty("dropWithProb")));

    NUM_PROCESSES = Integer.parseInt(overrideArgs.getOrDefault("numProcesses", prop.getProperty("numProcesses")));
    NUM_MAX_ROUNDS = Integer.parseInt(overrideArgs.getOrDefault("numMaxRounds", prop.getProperty("numMaxRounds")));
    NUM_MAX_MESSAGES = Integer.parseInt(overrideArgs.getOrDefault("numMaxMessages", prop.getProperty("numMaxMessages")));
    NUM_ROUNDS_IN_PROTOCOL = Integer.parseInt(overrideArgs.getOrDefault("numRoundsInProtocol", prop.getProperty("numRoundsInProtocol")));
    NUM_PHASES = Integer.parseInt(overrideArgs.getOrDefault("numPhases", prop.getProperty("numPhases")));
    NUM_REQUESTS = Integer.parseInt(overrideArgs.getOrDefault("numRequests", prop.getProperty("numRequests")));

    String[] workloadPointTokens = overrideArgs.getOrDefault("workloadPoints", prop.getProperty("workloadPoints")).split(",");
    workloadPoints = new int[workloadPointTokens.length];
    try{
      for(int i = 0; i < workloadPointTokens.length; i++)
        workloadPoints[i] = (Integer.parseInt(workloadPointTokens[i]));
    } catch(NumberFormatException e) {
      log.error("Workload points cannot be parsed. Please enter valid integers seperated by comma.");
      log.error(e.getMessage());
      System.exit(-1);
    }

    portNumber = Integer.parseInt(overrideArgs.getOrDefault("portNumber", prop.getProperty("portNumber")));
    NUM_CLIENTS = Integer.parseInt(overrideArgs.getOrDefault("numClients",prop.getProperty("numClients")));
    numAttempts = Integer.parseInt(overrideArgs.getOrDefault("numAttempts", prop.getProperty("numAttempts")));

    populationSize = Integer.parseInt(overrideArgs.getOrDefault("populationSize",prop.getProperty("populationSize")));
    elitesSize = Integer.parseInt(overrideArgs.getOrDefault("elitesSize",prop.getProperty("elitesSize")));
    numIterations = Integer.parseInt(overrideArgs.getOrDefault("numIterations",prop.getProperty("numIterations")));

    schedulerClass = overrideArgs.getOrDefault("scheduler", prop.getProperty("scheduler"));
    schedulerFile = overrideArgs.getOrDefault("schedulerFile", prop.getProperty("scheduleFile"));

    schedulerFileHasMsgContent = Boolean.parseBoolean(overrideArgs.getOrDefault("schedulerFileHasMsgContent", prop.getProperty("scheduleHasMsgContent")));

    libsDirectory = overrideArgs.getOrDefault("libsDirectory", prop.getProperty("libsDirectory"));
    initialDataDirectory = overrideArgs.getOrDefault("initialDataDirectory", prop.getProperty("initialDataDirectory"));
    runDirectory = overrideArgs.getOrDefault("runDirectory", prop.getProperty("runDirectory"));
    javaPath = overrideArgs.getOrDefault("javaPath", prop.getProperty("javaPath"));

    // Read cluster parameters
    clusterPort = Integer.parseInt(overrideArgs.getOrDefault("clusterPort", prop.getProperty("clusterPort")));
    poolTimeoutMillis = Integer.parseInt(overrideArgs.getOrDefault("poolTimeoutMillis", prop.getProperty("poolTimeoutMillis")));
    readTimeoutMillis = Integer.parseInt(overrideArgs.getOrDefault("readTimeoutMillis", prop.getProperty("readTimeoutMillis")));
    timeBetweenQueriesMillis = Integer.parseInt(overrideArgs.getOrDefault("timeBetweenQueriesMillis", prop.getProperty("timeBetweenQueriesMillis")));

    // Read logging parameters
    logResult = Boolean.parseBoolean(overrideArgs.getOrDefault("logResult", prop.getProperty("logResult")));
    logSchedule = Boolean.parseBoolean(overrideArgs.getOrDefault("logSchedule", prop.getProperty("logSchedule")));

    resultFolderName = overrideArgs.getOrDefault("resultFolder", prop.getProperty("resultFolder"));

    resultFileName = overrideArgs.getOrDefault("resultFile", prop.getProperty("resultFile"));

    maxExecutionDuration = Integer.parseInt(overrideArgs.getOrDefault("maxExecutionDuration", prop.getProperty("maxExecutionDuration")));
  }

  public WorkloadDirs getWorkloadDirs() {
    return new WorkloadDirs(libsDirectory, initialDataDirectory, runDirectory);
  }

  public void setRandomSeed(int randomSeed) {
    this.randomSeed = randomSeed;
  }

  public int getRandomSeed() {
    return randomSeed;
  }

  public void setResultFileName(String filename) {
    this.resultFileName = filename;
  }

  public void setResultFolderName(String folderName) {
    this.resultFileName = folderName;
    FileUtils.setOutputFolder(resultFolderName);
  }

  public void setSchedulerClass(String schedulerClassName) {
    this.schedulerClass = schedulerClassName;
  }

  public String getSchedulerClass() {
    return schedulerClass;
  }

  public String getResultFileName() {
    return resultFileName;
  }

  public int[] getWorkloadPoints() {
    return workloadPoints;
  }

  public void setWorkloadPoints(int[] workloadPoints) {
    if(this.workloadPoints.length != workloadPoints.length) {
      log.error("Cannot set workload points. Number of workloads is not equal to original count in the conf file.");
      System.exit(-1);
    }
    this.workloadPoints = workloadPoints;
  }

  public int getNumAttempts() {
    return numAttempts;
  }

  public void setNumAttempts(int numAttempts) {
    this.numAttempts = numAttempts;
  }

  private static Properties loadProperties(String configFile) {
    Properties prop = new Properties();
    try (FileInputStream ip = new FileInputStream(configFile)) {
      prop.load(ip);
    } catch (IOException e) {
      log.error("Can't load properties file: {}", configFile);
    }
    return prop;
  }

  public synchronized static ExplorerConf initialize(String configFile, String[] args) {
    INSTANCE = new ExplorerConf(configFile, args);
    return INSTANCE;
  }

  public synchronized static ExplorerConf getInstance() {
    if (INSTANCE == null) {
      throw new IllegalStateException("Configuration not initialized");
    }
    return INSTANCE;
  }

}
