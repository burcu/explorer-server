package explorer;

import explorer.message.PaxosMessage;
import explorer.net.Handler;
import explorer.net.MessageSender;
import explorer.scheduler.SchedulerProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnectionHandler implements Handler {

  private static final Logger log = LoggerFactory.getLogger(ConnectionHandler.class);
  private final SchedulerProxy schedulerProxy;

  public ConnectionHandler(SchedulerProxy schedulerProxy) {
    this.schedulerProxy = schedulerProxy;
  }

  @Override
  public void onConnect(int id, MessageSender sender) {
    log.info("Connected Node: " + id);
    synchronized (schedulerProxy) {
      schedulerProxy.onConnect(id, sender);
    }
  }

  @Override
  public void onDisconnect(int id) {
    log.info("Disconnected Node: " + id);
    synchronized (schedulerProxy) {
      schedulerProxy.onDisconnect(id);
    }
  }

  @Override
  public void onReceive(int id, String message) {
    //log.debug("==Received from Node: " + id + " Message: " + message );

    synchronized (schedulerProxy) {
      PaxosMessage event = PaxosMessage.toObject(message);

      if(event.isAckEvent())
        schedulerProxy.addAckEvent((int)event.getSender(), event); // the sender of the ack, event
      else  {
        log.debug("==Received from Node: " + id + " Message: " + message );
        schedulerProxy.addNewEvent(id, event);
      }
    }
  }
}
