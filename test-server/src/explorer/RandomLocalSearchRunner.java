package explorer;

import explorer.fuzz.GAPopulation;
import explorer.fuzz.fitness.Fitness;
import explorer.fuzz.fitness.MinimalBuggyFitness;
import explorer.schedule.Schedule;
import org.slf4j.LoggerFactory;


/**
 * Extends TestRunner with a ScheduleExplorer
 *    that performs randomized local search around a given schedule
 *    selecting best schedules among a population of randomly mutated schedules (using MinimalBuggyFitness)
 */
public class RandomLocalSearchRunner extends TestRunner {
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(RandomLocalSearchRunner.class);

    private final GAPopulation scheduleExplorer;

    public RandomLocalSearchRunner(ExplorerConf conf) {
        super(conf);
        scheduleExplorer = new GAPopulation(conf.getRandomSeed(), conf.populationSize, conf.elitesSize, conf.NUM_MAX_MESSAGES);
        logToResultsFile(scheduleExplorer.getConfigAsStr());
    }

    /**
     * Explores around the schedule for given number of iterations, with given number of population
     * @param schedule to random search around
     */
    public void randomLocalSearch(Schedule schedule) {

        scheduleExplorer.updatePopulation(schedule);

        for(int currIteration = 1; currIteration <= conf.numIterations; currIteration++) {

            log.info("Running population " + currIteration + " with seed: " + conf.getRandomSeed());
            conf.setResultFileName(conf.getResultFileName() + currIteration);
            logToResultsFile("Running population " + currIteration + " with seed: " + conf.getRandomSeed());

            for(int testId = ((currIteration-1) * conf.populationSize) + 1; testId <= currIteration * conf.populationSize; testId ++) {
                Schedule s = scheduleExplorer.getSchedule((testId - 1) % conf.populationSize);
                TestResult tr = runTestSchedule(testId, s);

                Fitness fitness = new MinimalBuggyFitness(currentScheduler.getScheduledMessages(), currentScheduler.getToScheduleMessages(), conf.NUM_PROCESSES, !tr.verified);
                //  update schedule "getSchedule(index) with what's executed. It may be larger
                // (e.g. with higher # events and hence larger priorities/schedule)"
                scheduleExplorer.addExplored(s, fitness);
                scheduleExplorer.setSchedule(((testId - 1) % conf.populationSize), s); // currently all schedules have the same size
                //logToResultsFile(currentScheduler.getStats() + fitness + verifier.getResultString());
            }

            // update the population of schedules to be explored
            scheduleExplorer.updatePopulation(true);
        }
    }

}