package explorer.workload;

import java.util.List;

public interface WorkloadDriver {
  void prepare(int testId);
  void startEnsemble();
  void sendWorkload(); // bulk workload to reproduce a buggy scenario (e.g. with multiple queries)
  void submitQuery(int nodeId, String query);
  void submitQuery(int nodeId, int queryId); // runs the query in a workload configuration file
  void submitQueries(List<Integer> nodeIds, List<String> queries);
  void sendResetWorkload();
  void prepareNextTest();
  void stopEnsemble();
  void cleanup();
}
