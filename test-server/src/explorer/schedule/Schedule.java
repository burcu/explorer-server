package explorer.schedule;

import explorer.message.PaxosMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class Schedule {

  // change the order of an event, remove and insert to a random position
  public abstract Schedule mutateRandomEvent(Random random);
  // swap the order of two events
  public abstract Schedule mutateRandomEventPair(Random random);

  public abstract Schedule dropRandomEvent(Random random);

  // static utility methods for lists of messages:

  public static boolean inOrder(PaxosMessage message, EventListSchedule schedule) {
    return inOrder(message, schedule.getMessages());
  }

  public static boolean inOrder(PaxosMessage message, List<PaxosMessage> messages) {
    int index  = messages.indexOf(message);
    for(int i = 0; i < index; i++) {
      PaxosMessage m = messages.get(i);
      if(message.hasSmallerBallot(m)
              || (message.getBallot().equals(m.getBallot()) && message.getProtocolStep() < m.getProtocolStep()))
        return false;
    }
    return true;
  }

  public static List<PaxosMessage> getAllOOOMessages(List<PaxosMessage> messages) {
    List<PaxosMessage> ooo = new ArrayList<>();
    PaxosMessage mostRecent = messages.get(0);
    for(PaxosMessage m: messages) {
      if (m.hasSmallerBallot(mostRecent) || (m.getBallot().equals(mostRecent.getBallot()) && m.getProtocolStep() < mostRecent.getProtocolStep())) {
        ooo.add(m);
      } else {
        mostRecent = m;
      }
    }
    return ooo;
  }

}
