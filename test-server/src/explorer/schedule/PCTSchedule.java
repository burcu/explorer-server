package explorer.schedule;

import explorer.ExplorerConf;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PCTSchedule extends Schedule {

    private List<Integer> priorityChainPositionsInserted; // priorities of chain id's
    private List<Integer> priorityChangePoints;

    public PCTSchedule(List<Integer> priorityChainPositionsInserted, List<Integer> priorityChangePoints) {
        this.priorityChainPositionsInserted = priorityChainPositionsInserted;
        this.priorityChangePoints = priorityChangePoints;
    }

    public List<Integer> getPriorityChainPositions() {
        return priorityChainPositionsInserted;
    }

    public void setPriorityChainPositions(List<Integer> chainPriorities) {
        this.priorityChainPositionsInserted = chainPriorities;
    }

    public List<Integer> getPriorityChangePoints() {
        return priorityChangePoints;
    }

    public void setPriorityChangePoints(List<Integer> priorityChangePoints) {
        this.priorityChangePoints = priorityChangePoints;
    }

    @Override
    public String toString() {
        return "PCTSchedule{" +
                "chainPriorities=" + priorityChainPositionsInserted +
                ", priorityChangePoints=" + priorityChangePoints +
                '}';
    }

    /**
     * Mutate one of the events in the tuple (priority change points) randomly (e1..e(d-1))
     * OR
     * Mutate the priority of one of the chains (change e0)
     */
    @Override
    public Schedule mutateRandomEvent(Random random) {
        // TODO clean and rename mutations!
        // change a strongly hit event
        int index = random.nextInt(priorityChangePoints.size());
        int randomEvent = random.nextInt(ExplorerConf.getInstance().NUM_MAX_MESSAGES);

        while(priorityChangePoints.contains(randomEvent)) {
            randomEvent = random.nextInt(ExplorerConf.getInstance().NUM_MAX_MESSAGES);
        }

        List<Integer> mutatedPriorityChangePts = new ArrayList<>(priorityChangePoints);
        mutatedPriorityChangePts.remove(index);
        mutatedPriorityChangePts.add(index, randomEvent);

        return new PCTSchedule(new ArrayList<>(priorityChainPositionsInserted), mutatedPriorityChangePts);
    }

    /**
     * Swap two events in the  priority change points randomly
     */
    @Override
    public Schedule mutateRandomEventPair(Random random) {
        return this;  //TODO
    }

    @Override
    public Schedule dropRandomEvent(Random random) {
        return this;  //TODO
    }
}
