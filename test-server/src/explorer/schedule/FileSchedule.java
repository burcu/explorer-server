package explorer.schedule;

import java.util.Random;

/**
 * Schedule (messages to be scheduled or dropped) is recorded and given in a file
 * Executed by FileReplayingScheduler
 */
public class FileSchedule extends Schedule {

    private final String scheduleFile;

    public FileSchedule(String scheduleFileName) {
        this.scheduleFile = scheduleFileName;
    }

    public String getScheduleFile() {
        return scheduleFile;
    }

    @Override
    public Schedule mutateRandomEvent(Random random) { //TODO
        return this;
    }

    @Override
    public Schedule mutateRandomEventPair(Random random) { //TODO
        return this;
    }

    @Override
    public Schedule dropRandomEvent(Random random) { //TODO
        return this;
    }
}
