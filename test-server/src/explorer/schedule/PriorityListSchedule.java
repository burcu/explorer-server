package explorer.schedule;

import java.util.*;

public class PriorityListSchedule extends Schedule {
  private final List<Double> priorities;

  public PriorityListSchedule(Random random, int size) {
    priorities = new ArrayList<>();
    for(int i = 0; i < size; i++) {
      priorities.add(random.nextDouble());
    }
  }

  public PriorityListSchedule(List<Double> priorities) {
    this.priorities = priorities;
  }

  public PriorityListSchedule mutateIndex(Random random, int index) {
    List<Double> copy1, copy2;
    double newValue;
    List<Double> mutated;

    do{
      mutated = new ArrayList<>(priorities);
      newValue = random.nextDouble();
      mutated.set(index, newValue);

      copy1 = new ArrayList<>(priorities);
      Collections.sort(copy1);
      copy2 = new ArrayList<>(mutated);
      Collections.sort(copy2);
    } while(copy1.indexOf(priorities.get(index)) == copy2.indexOf(newValue));
    // repeat if mutation is equivalent to the original schedule, try again

    return new PriorityListSchedule(mutated);
  }

  public boolean isEmpty() {
    return priorities.isEmpty();
  }

  public PriorityListSchedule mutateRandomEvent(Random random) {
    List<Double> mutated = new ArrayList<>(priorities);
    int index = random.nextInt(priorities.size());
    double newValue = random.nextDouble();
    mutated.set(index, newValue);
    return new PriorityListSchedule(mutated);
  }

  public PriorityListSchedule mutateRandomEventPair(Random random) {
    List<Double> mutated = new ArrayList<>(priorities);
    int index1 = random.nextInt(priorities.size());
    int index2 = random.nextInt(priorities.size());
    double swap = priorities.get(index1);
    mutated.set(index1, priorities.get(index2));
    mutated.set(index2, swap);
    return new PriorityListSchedule(mutated);
  }

  @Override
  public Schedule dropRandomEvent(Random random) {
    return null;
  }

  public List<Double> getPriorities() {
    return priorities;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    if(priorities.size() != ((PriorityListSchedule) o).priorities.size()) return false;

    for(int i = 0; i < priorities.size(); i++) {
      if(!priorities.get(i).equals(((PriorityListSchedule) o).priorities.get(i)))
        return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    return Objects.hash(priorities);
  }
}
