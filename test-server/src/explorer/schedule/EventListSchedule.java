package explorer.schedule;

import explorer.ExplorerConf;
import explorer.fuzz.fitness.OOOCountFitness;
import explorer.message.PaxosMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class EventListSchedule extends Schedule {

    private final List<PaxosMessage> messages;
    private final OOOCountFitness fitness;

    public EventListSchedule(List<PaxosMessage> messages) {
        this.messages = new ArrayList<>();
        this.messages.addAll(messages);
        this.fitness = new OOOCountFitness(messages, new ArrayList<>(), ExplorerConf.getInstance().NUM_PROCESSES);
    }

    public EventListSchedule(EventListSchedule schedule) {
        this.messages = new ArrayList<>();
        messages.addAll(schedule.getMessages());
        this.fitness = new OOOCountFitness(messages, new ArrayList<>(), ExplorerConf.getInstance().NUM_PROCESSES);
    }

    public boolean isEmpty() {
        return messages.isEmpty();
    }

    @Override
    public Schedule mutateRandomEvent(Random random) {
        List<PaxosMessage> mutated = new ArrayList<>(messages);
        int oldIndex = random.nextInt(messages.size());
        int newIndex;
        do{
            newIndex = random.nextInt(messages.size());
        } while(newIndex == oldIndex);
        PaxosMessage m = mutated.remove(oldIndex);
        mutated.add(newIndex, m);
        return new EventListSchedule(mutated);
    }

    @Override
    public Schedule mutateRandomEventPair(Random random) {
        List<PaxosMessage> mutated = new ArrayList<>(messages);
        int index1 = random.nextInt(messages.size()-1);
        PaxosMessage e = mutated.remove(index1);
        mutated.add(index1+1, e);
        return new EventListSchedule(mutated);
    }

    public Schedule dropRandomEvent(Random random) {
        List<PaxosMessage> mutated = new ArrayList<>(messages);
        int index = random.nextInt(messages.size());
        mutated.remove(index);
        return new EventListSchedule(mutated);
    }

    public Schedule mutateRandomOOO(Random random) {
        List<PaxosMessage> mutated = new ArrayList<>(messages);
        int index1 = random.nextInt(fitness.getOooMessages().size());
        PaxosMessage m = fitness.getOooMessages().get(index1);
        mutated.remove(m);
        int index2 = random.nextInt(messages.size());
        mutated.add(index2, m);
        return new EventListSchedule(mutated);
    }

    public Schedule dropRandom000(Random random) {
        List<PaxosMessage> mutated = new ArrayList<>(messages);
        int index = random.nextInt(fitness.getOooMessages().size());
        mutated.remove(fitness.getOooMessages().get(index));
        return new EventListSchedule(mutated);
    }

    public OOOCountFitness getFitness() {
        return fitness;
    }

    public List<PaxosMessage> getMessages() {
        return new ArrayList<>(messages);
    }

    /**
     *
     * @return true if there exists a message *on the same receiver*  in a smaller round scheduled earlier
     */
    public List<PaxosMessage> getOOOMessages() {
        return fitness.getOooMessages();
    }

}
