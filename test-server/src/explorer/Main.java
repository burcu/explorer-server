package explorer;

import explorer.message.PaxosMessage;
import explorer.schedule.*;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class Main {
    private static final org.slf4j.Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        BasicConfigurator.configure();
        Logger.getRootLogger().setLevel(Level.INFO);
        ExplorerConf conf = ExplorerConf.initialize("explorer.conf", args);

        fuzzPCTSchedules(conf);

        //runSchedulesFromFile(conf);
        //exploreAroundPCTSchedules(conf);
        //exploreAroundSyncSchedule(conf);
        //exploreAroundPOSSchedule(conf);
    }

    public static void fuzzPCTSchedules(ExplorerConf conf) {
        conf.setResultFileName("fuzz-" + conf.getRandomSeed() + ".txt");

        // Test scenario settings
        conf.setNumAttempts(0); // no retries for failing client requests
        int[] worklaodPoints = {15,29};
        conf.setWorkloadPoints(worklaodPoints);

        FuzzSearchRunner testRunner = new FuzzSearchRunner(conf);
        testRunner.fuzzSearch();
        testRunner.stopTestServer();
    }


    // Example uses of different types of TestRunners/Explorers with different types of Schedules:

    // SystematicLocalSearchRunner or RandomLocalSearchRunner
    // Local (systematic or random) search around a schedule given in a file
    public static void exploreAroundPCTSchedules(ExplorerConf conf) {
        conf.setResultFileName("exampleAroundSyncSchedule.txt");
        String[] taPCTSchedules = { "d6s12346383", "d4s12345994", "d4s12346674", "d5s12345805", "d5s12346117",
                "d5s12346625", "d6s12346383", "d6s12346388", "d6s12346534"};

        // Test scenario settings for (ta)PCT schedules reproducing the bugs
        conf.setNumAttempts(0); // no retries for failing client requests
        int[] worklaodPoints = {15,29};
        conf.setWorkloadPoints(worklaodPoints);

        RandomLocalSearchRunner testRunner = new RandomLocalSearchRunner(conf);
        //SystematicLocalSearchRunner testRunner = new SystematicLocalSearchRunner(conf); // TODO add to repo!

        for(String s: taPCTSchedules) {
            conf.setResultFileName("out-" + s);
            String fileName = "schedules/taPCT/" + s + ".txt";
            testRunner.logToResultsFile("Replaying file: " + fileName);

            List<PaxosMessage> messages = testRunner.runTestSchedule(1, new FileSchedule(fileName)).messages;
            if(messages != null) {
                EventListSchedule originalSchedule = new EventListSchedule(messages);
                testRunner.randomLocalSearch(originalSchedule);
                //testRunner.systematicLocalSearch(originalSchedule);
            }
        }

        testRunner.stopTestServer();
    }


    // Example: Running schedules from files:
    public static void runSchedulesFromFile(ExplorerConf conf) {
        conf.setResultFileName("resultsFromFiles.txt");
        String[] scheduleFiles = { "d4s12345994", "d4s12346674", "d5s12345805", "d5s12346117",
                "d5s12346625", "d6s12346383", "d6s12346388", "d6s12346534"};

        // Test scenario settings for (ta)PCT schedules reproducing the bugs
        conf.setNumAttempts(0); // no retries for failing client requests
        int[] worklaodPoints = {15,29};
        conf.setWorkloadPoints(worklaodPoints);

        TestRunner testRunner = new TestRunner(conf);
        for(String s: scheduleFiles) {
            String fileName = "schedules/taPCT/" + s + ".txt";
            testRunner.logToResultsFile("Replaying file: " + fileName);
            testRunner.runTestSchedule(1, new FileSchedule(fileName));
        }
        testRunner.stopTestServer();
    }

}
