package explorer.scheduler;

import explorer.Event;
import explorer.message.PaxosMessage;
import explorer.schedule.EventListSchedule;
import explorer.schedule.Schedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Replays the schedule of events given in a list
 * The messages are matched in full content - ballot, sender/receiver, payload
 * Enforces the execution of the specified schedule - DOES NOT DEVIATE
 */
public class ListReplayingScheduler extends Scheduler {

  private static final Logger log = LoggerFactory.getLogger(FileReplayingScheduler.class);

  private final List<PaxosMessage> scheduleToReplay;
  private List<Event> eventsToSchedule;
  private PaxosMessage nextMessage;

  public ListReplayingScheduler(List<PaxosMessage> messages) {
    scheduleToReplay = new ArrayList<>();
    scheduleToReplay.addAll(messages);

    initStructures();
  }

  public void initStructures() {
    scheduled.clear();
    eventsToSchedule = new ArrayList<>();
    nextMessage = scheduleToReplay.get(scheduled.size());
  }

  @Override
  synchronized public void addNewEvents(List<Event> events) {
    super.addNewEvents(events);
    eventsToSchedule.addAll(events);
  }

  public synchronized Event scheduleNext() {
    int numTrials = 0;

    while(numTrials < 10) {
      for(Event e: eventsToSchedule) {
        if(isOkToSchedule(e.getMessage())) {
          eventsToSchedule.remove(e);
          schedule(e);
          if(!isScheduleCompleted()) nextMessage = scheduleToReplay.get(scheduled.size());
          return e;
        }
      }

      // next message is not available
      try {
        Thread.sleep(250);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      numTrials ++;
    }

    log.error("Next message: " + nextMessage + " is not available");

    return null;
  }

  private boolean isOkToSchedule(PaxosMessage message) {
    return nextMessage.getMessageContentWithBallot().equals(message.getMessageContentWithBallot());
  }

  public boolean isScheduleCompleted() {
    return scheduled.size() == scheduleToReplay.size();
  }

  @Override
  public Schedule getSchedule() {
    return new EventListSchedule(scheduleToReplay);
  }

  @Override
  public String getStats() {
    return super.getStats();
  }

}
