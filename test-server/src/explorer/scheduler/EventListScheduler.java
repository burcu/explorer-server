package explorer.scheduler;

import explorer.Event;
import explorer.message.PaxosMessage;
import explorer.schedule.EventListSchedule;
import explorer.schedule.Schedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Runs the schedule of events given in a list
 * Enforces the execution of the specified schedule
 *    - If the next event in the list is not enabled,
 *      deviates from the schedule by randomly selecting one of the enabled events
 */
public class EventListScheduler extends Scheduler {

    private static final Logger log = LoggerFactory.getLogger(EventListScheduler.class);

    private final List<PaxosMessage> scheduleToReplay;
    private PaxosMessage messageToOrder;
    private PaxosMessage nextMessage;
    private final Random random; // for random deviations from input schedule if necessary

    private int nextEventNotEnabledCount = 0;
    private final int eventCount;

    public EventListScheduler(EventListSchedule schedule, int randomSeed) {
        scheduleToReplay = schedule.getMessages();
        eventCount = scheduleToReplay.size();
        messageToOrder = null;
        random = new Random(randomSeed);
        initStructures();
    }

    public void initStructures() {
        scheduled.clear();
        nextMessage = scheduleToReplay.get(0);
    }

    @Override
    synchronized public void addNewEvents(List<Event> events) {
        super.addNewEvents(events);

        //Current version does not add newcomers (events that do not appear in the schedule to be replayed)
        /*
        for(Event e: events) {
            if(!scheduleToReplay.contains(e.getMessage())) {
                int r = random.nextInt(scheduleToReplay.size() - scheduled.size()) + scheduled.size();
                scheduleToReplay.add(r, e.getMessage());
                //scheduleToReplay.add(e.getMessage());
            }
        }*/

    }

    private boolean isSmallest(PaxosMessage message, List<PaxosMessage> messages) {
        for(PaxosMessage m: messages) {
            if(m.hasSmallerBallot(message))
                return false;
        }
        return true;
    }

    public synchronized Event scheduleNext() {
        if(messageToOrder != null) {
            for(Event e: toSchedule) {
                if(e.getMessage().hasSameContent(messageToOrder) && isSmallest(messageToOrder, getToScheduleMessages())) {
                    schedule(e);
                    scheduleToReplay.remove(messageToOrder);
                    scheduleToReplay.add(scheduled.size()-1, messageToOrder);
                    messageToOrder = null;
                    nextMessage = scheduleToReplay.get(scheduled.size());
                    return e;
                }
            }
        }

        for(Event e: toSchedule) {
            if(e.getMessage().hasSameContent(nextMessage)) {
                schedule(e);
                if(!isScheduleCompleted()) nextMessage = scheduleToReplay.get(scheduled.size());
                return e;
            }
        }

        System.out.println("----- Not enabled: " + nextMessage.toString() + " numScheduled: " + scheduled.size());
        nextEventNotEnabledCount ++;

        // next event not there - test set 1 - included the block below
        int index = scheduled.size();
        while(!toSchedule.isEmpty() && index < scheduleToReplay.size() - 1) {
            for(Event e: toSchedule) {
                if(e.getMessage().hasSameContent(nextMessage)) {
                    schedule(e);
                    if(!isScheduleCompleted()) nextMessage = scheduleToReplay.get(scheduled.size());
                    return e;
                }
            }
            index ++;
        }

        if(!toSchedule.isEmpty()) {
            // the event in the input list is not available, randomly schedule another event
            int next = random.nextInt(toSchedule.size());
            Event e = toSchedule.get(next);
            schedule(e);
            return e;
        }
        return null;
    }


    public boolean isScheduleCompleted() {
        return scheduled.size() == eventCount;
    }

    @Override
    public Schedule getSchedule() {
        return new EventListSchedule(getScheduledMessages());
    }

    @Override
    public String getStats() {
        return super.getStats() + "Num scheduled: " + scheduled.size() + "\nNot enabled count: " + nextEventNotEnabledCount + "\n";
    }

}

