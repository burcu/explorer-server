package explorer.scheduler;

import explorer.Event;
import explorer.ExplorerConf;
import explorer.message.PaxosMessage;
import explorer.schedule.Schedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;

public abstract class Scheduler {
  private static Logger log = LoggerFactory.getLogger(Scheduler.class);
  protected SchedulerSettings settings;
  ExplorerConf conf = ExplorerConf.getInstance();

  protected List<Event> toSchedule;
  protected ConcurrentLinkedQueue<Event> scheduled;
  protected ConcurrentLinkedQueue<Event> dropped;

  protected List<String> ballots = new ArrayList<>();
  protected List<String> commits = new ArrayList<>();

  public Scheduler() {
    initStructures();
  }

  public Scheduler(SchedulerSettings settings) {
    this.settings = settings;
    initStructures();
  }

  private void initStructures() {
    toSchedule = new ArrayList<>();
    scheduled = new ConcurrentLinkedQueue<>();
    dropped = new ConcurrentLinkedQueue<>();
  }

  public void addNewEvents(List<Event> events) {
    Event.sortEvents(events);
    toSchedule.addAll(events);

    for(Event m: events) {
      log.debug("Added: " + m.getMessage().getMessageContentWithSeqNumber());
      //FileUtils.writeToFile(conf.resultFile, "Added: " + m.getMessage().getMessageContentWithSeqNumber(), true);
    }

    for(Event m: events) {
      if(m.getMessage().getVerb().equals("PAXOS_PREPARE") && !ballots.contains(m.getMessage().getPayloadBallot())) {
        ballots.add(m.getMessage().getPayloadBallot());
      } else if(m.getMessage().getVerb().equals("PAXOS_COMMIT") && !commits.contains(m.getMessage().getPayloadBallot())) {
        commits.add(m.getMessage().getPayloadBallot());
      }
    }
  }

  /**
   * @return true if an event is scheduled
   */
  public abstract Event scheduleNext();

  /**
   * Can be overridden for additional bookkeeping and operations
   * @return
   */
  public synchronized void schedule(Event event) {
    scheduled.add(event);
    toSchedule.remove(event);
  }

  /**
   * Can be overridden for additional bookkeeping and operations
   * @return
   */
  public synchronized void drop(Event event) {
    dropped.add(event);
    toSchedule.remove(event);
  }

  public int getNumAllEvents() {
    return toSchedule.size() + scheduled.size() + dropped.size();
  }
  /**
   * Can be overriden to add additional requirements
   * @return
   */
  public synchronized boolean isScheduleCompleted() {
    return toSchedule.isEmpty();
  }

  public List<Event> getScheduledEvents() {
    return new ArrayList<>(scheduled);
  }

  public List<PaxosMessage> getScheduledMessages() {
    List<PaxosMessage> messages = new ArrayList<>();
    for(Event e: scheduled) {
      messages.add(e.getMessage());
    }
    return messages;
  }

  public List<PaxosMessage> getToScheduleMessages() {
    List<PaxosMessage> messages = new ArrayList<>();
    for(Event e: toSchedule) {
      messages.add(e.getMessage());
    }
    return messages;
  }

  public abstract Schedule getSchedule();

  public String getScheduleAsStr() {
    StringBuilder sb = new StringBuilder("Schedule: ");
    for(Event e: scheduled) {
      sb.append("\n").append(e.getMessage().getMessageContent()).append(" ").append(e.getMessage().getPayload());
    }
    return sb.toString();
  }

  public String getDroppedAsStr() {
    StringBuilder sb = new StringBuilder("\nDropped: ");
    for(Event e: dropped) {
      sb.append("\n").append(e.getMessage().getMessageContentWithSeqNumber()).append(" ").append(e.getMessage().getPayload());
    }
    return sb.toString();
  }

  public String getStats() {
    StringBuilder sb = new StringBuilder();
    if(conf.logSchedule) sb.append(getScheduleAsStr()).append("\n");
    //if(conf.logSchedule) sb.append(getDroppedAsStr()).append("\n");
    return sb.toString();
  }

}
