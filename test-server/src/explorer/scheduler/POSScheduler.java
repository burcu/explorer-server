package explorer.scheduler;

import explorer.Event;
import explorer.ExplorerConf;
import explorer.schedule.PriorityListSchedule;
import explorer.schedule.Schedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class POSScheduler extends Scheduler {
  private static final Logger log = LoggerFactory.getLogger(PCTScheduler.class);
  ExplorerConf conf = ExplorerConf.getInstance();

  private Map<Event, Double> eventPriorities; // assigned randomly
  private List<Double> priorities;
  private int numAddedEvents; // to get the priority preset to an indexed event

  private Random random;

  private final boolean setPrioritiesOnline;

  private List<String> commitBallots = new ArrayList<>();

  public POSScheduler() {
    initStructures();
    priorities = new ArrayList<>();
    setPrioritiesOnline = true;
  }

  public POSScheduler(Schedule schedule) {
    initStructures();

    if(!(schedule instanceof PriorityListSchedule)) {
      log.error("Wrong schedule type for POSScheduler");
      System.exit(-1);
    }

    this.priorities = ((PriorityListSchedule) schedule).getPriorities();

    setPrioritiesOnline = false;
  }

  public POSScheduler(List<Double> priorities) {
    initStructures();
    this.priorities = priorities;
    setPrioritiesOnline = false;
  }

  public void initStructures() {
    random = new Random(conf.getRandomSeed());
    eventPriorities = new HashMap<>();
    numAddedEvents = 0;
  }

  @Override
  public void addNewEvents(List<Event> events) {
    super.addNewEvents(events);

    for(Event e: events) {
      if(!eventPriorities.containsKey(e)) {
        double p;
        if(setPrioritiesOnline) {
          p = random.nextDouble();
          priorities.add(p);
        } else {
          if(priorities.size() > numAddedEvents)
            p = priorities.get(numAddedEvents); //ADDED event's size!
          else {
            p = random.nextDouble();
            priorities.add(p);
          }
        }
        eventPriorities.put(e, p);
        numAddedEvents ++;
        //logger.debug("--- Added transition: " + t.getId() + " with id: " + Event.getEventId(t) + " with priority: " + p);
      }
    }
  }

  @Override
  public Event scheduleNext() {
    Event next = getHighestPriority();
    if(next != null) {
      eventPriorities.remove(next);
      schedule(next);
      if(next.getMessage().getProtocolStep() == 4)
        commitBallots.add(next.getMessage().getBallot());
    }
    return next;
  }


  private Event getHighestPriority() {
    double max = 0;
    Event emax = null;

    for(Event e: eventPriorities.keySet()) {
      if (eventPriorities.get(e) > max) {
        max = eventPriorities.get(e);
        emax = e;
      }
    }
    return emax;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("POS Scheduler using: ");
    sb.append("Seed: ").append(conf.getRandomSeed()).append("  ");
    return sb.toString();
  }

  @Override
  public String getStats() {
    StringBuilder sb = new StringBuilder("\nPOS with: ");
    if(setPrioritiesOnline) sb.append("Seed: ").append(conf.getRandomSeed()).append("\n");
    sb.append("Priorities: ");
    for(double d: priorities)
      sb.append(String.format("%.2f", d)).append(",");
    sb.append("\n#Scheduled: ").append(getScheduledMessages().size());

    sb.append(" ").append(super.getStats());
    return sb.toString();
  }

  public Schedule getSchedule() {
    return new PriorityListSchedule(new ArrayList<>(priorities));
  }

  @Override
  public synchronized boolean isScheduleCompleted() {
    //boolean result = commitBallots.stream().distinct().count() >= 3;
    //if(result) System.out.println("here I return True");
    //return result;
    return toSchedule.isEmpty() && scheduled.size() >= conf.NUM_MAX_MESSAGES;
  }
/*
  private int numCommitsByAtLeastTwo() {
    for(String s: )
  }*/
}
