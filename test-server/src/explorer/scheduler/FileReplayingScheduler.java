package explorer.scheduler;

import explorer.Event;
import explorer.ExplorerConf;
import explorer.schedule.FileSchedule;
import explorer.schedule.Schedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Replays the schedule of events given in a file
 * The file does not need to list full content of messages - sender/receiver protocol message type is sufficient
 * Enforces the execution of the specified schedule - DOES NOT DEVIATE
 * (The schedule can also specify the messages to be dropped)
 */
public class FileReplayingScheduler extends Scheduler {

  private static final Logger log = LoggerFactory.getLogger(FileReplayingScheduler.class);

  private final String fileName;
  private final List<String> scheduleToReplay;
  private final List<String> eventsToDrop;
  private Map<String, Event> eventsToSchedule;
  private String nextMessage;

  private final String DROP_MARK = "@D";

  public FileReplayingScheduler(FileSchedule schedule) {
    log.info("Replaying file: " + schedule.getScheduleFile());
    fileName = schedule.getScheduleFile();
    scheduleToReplay = readSchedule(fileName);
    eventsToDrop = readEventsToDrop();
    initStructures();
  }

  public FileReplayingScheduler(String scheduleFile) {
    log.info("Replaying file: " + scheduleFile);
    fileName = conf.schedulerFile;
    scheduleToReplay = readSchedule(conf.schedulerFile);
    eventsToDrop = readEventsToDrop();
    initStructures();
  }

  public void initStructures() {
    scheduled.clear();
    eventsToSchedule = new HashMap<>();
    nextMessage = scheduleToReplay.get(0);
  }

  private List<String> readSchedule(String schedulerFile) {
    try {
      return Files.readAllLines(Paths.get(schedulerFile))
          .stream()
          .map(String::trim)
          .filter(s -> !s.isEmpty() && !s.startsWith("//"))
          .map(s -> s.split("//")[0].trim())
          .collect(Collectors.toList());
    } catch (IOException e) {
      log.error("Can't read schedule file", e);
    }
    return new ArrayList<>();
  }

  private List<String> readEventsToDrop() {
    try {
      return Files.readAllLines(Paths.get(fileName))
          .stream()
          .map(String::trim)
          .filter(s -> !s.isEmpty() && s.startsWith("//"))
          //.map(s -> s.split("//")[1].trim())
          .filter(s -> s.split("//").length > 0 )
          .filter(s -> s.split("//")[1].trim().startsWith(DROP_MARK))
          .map(s -> s.split("//")[1].trim().split(DROP_MARK)[1].trim())
          .collect(Collectors.toList());
    } catch (IOException e) {
      log.error("Can't read schedule file", e);
      System.exit(-1);
    }
    return new ArrayList<>();
  }

  @Override
  synchronized public void addNewEvents(List<Event> events) {
    super.addNewEvents(events);
    for(Event m: events) {
      if(!eventsToDrop.contains(m.getMessage().toString())) {
        if(ExplorerConf.getInstance().schedulerFileHasMsgContent)
          eventsToSchedule.put(m.getMessage().getMessageContent() + " " + m.getMessage().getPayload(), m);
        else
          eventsToSchedule.put(m.getMessage().getMessageContent(), m);
      }
    }
  }

  public synchronized Event scheduleNext() {

    for(String s: eventsToSchedule.keySet()) {
      if(isOkToSchedule(s)) {
        Event m = eventsToSchedule.get(s);
        eventsToSchedule.remove(s);
        schedule(m);
        if(!isScheduleCompleted()) nextMessage = scheduleToReplay.get(scheduled.size());
        return m;
      }
    }

    return null;
  }

  private boolean isOkToSchedule(String eventId) {
    return nextMessage.equals(eventId);
  }

  public boolean isScheduleCompleted() {
    return scheduled.size() == scheduleToReplay.size();
  }

  @Override
  public Schedule getSchedule() {
    return new FileSchedule(fileName);
  }

  @Override
  public String getStats() {
    return super.getStats();
  }

}
