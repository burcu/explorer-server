package explorer.scheduler;

import java.util.Arrays;
import java.util.Objects;

public class VectorClock {

  private int[] vector;
  private int numProcesses;

  public VectorClock(int numProcesses) {
    this.numProcesses = numProcesses;
    vector = new int[numProcesses];
  }

  // called whenever the process with this vector clock receives a message
  public void updateWith(VectorClock received, int receiverId) {
    increase(receiverId);
    for(int i = 0; i < numProcesses; i++) {
      if(this.vector[i] < received.vector[i]) {
        this.vector[i] = received.vector[i];
      }
    }
  }

  public void increase(int index) {
    vector[index] ++;
  }

  public boolean isBefore(VectorClock other) {
    for (int i = 0; i < numProcesses; ++i) {
      if (vector[i] > other.vector[i]) {
        return false;
      }
    }
    return true;
  }

  public boolean isConcurrent(VectorClock other) {
    return !isBefore(other) && !other.isBefore(this);
  }

  public VectorClock clone() {
    VectorClock cloneVC = new VectorClock(numProcesses);
    if (numProcesses >= 0) System.arraycopy(vector, 0, cloneVC.vector, 0, numProcesses);
    return cloneVC;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    VectorClock that = (VectorClock) o;
    return numProcesses == that.numProcesses && Arrays.equals(vector, that.vector);
  }

  @Override
  public int hashCode() {
    int result = Objects.hash(numProcesses);
    result = 31 * result + Arrays.hashCode(vector);
    return result;
  }

  public String toString() {
    String result = "[";
    for (int i = 0; i < numProcesses; i++) {
      result = result.concat(" " + vector[i]);
    }
    result = result.concat("]");
    return result;
  }
}
