package explorer.scheduler;

import com.google.gson.annotations.Expose;
import explorer.ExplorerConf;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class NodeFailureSettings extends SchedulerSettings {
  private static final Logger log = LoggerFactory.getLogger(NodeFailureSettings.class);
  ExplorerConf conf = ExplorerConf.getInstance();

  // derived from the parameters
  public final int NUM_MAJORITY= (conf.NUM_PROCESSES / 2) + 1;

  private Random random;
  public final int depth = conf.bugDepth;

  @Expose
  public int seed = conf.getRandomSeed();
  @Expose
  private List<NodeFailureSettings.NodeFailure> failures = new ArrayList<>();
  @Expose
  public int mutationNo = 0; // the mutations of the same seed are enumerated for logging

  private int numMutators = 0;

  // to be used for deserialization (failures will be set)
  public NodeFailureSettings() {
    this(ExplorerConf.getInstance().getRandomSeed());
  }

  // to be used for creation
  public NodeFailureSettings(int seed) {
    this.seed = seed;
    random = new Random(seed);
    failures = getRandomFailures();
    //failures = getFailuresToReproduceBug();
    String failuresAsStr = toString();
    log.info("Failure Injecting Settings: \n" + failuresAsStr);
  }

  public NodeFailureSettings(List<NodeFailure> failures) {
    this.failures = failures;
  }

  // constructor used when constructed from a mutation - written as json for next executions
  private NodeFailureSettings(int seed, List<NodeFailure> failures, int mutationNo) {
    random = new Random(seed);
    this.mutationNo = mutationNo;
    this.failures = failures;
  }

  /**
   * Not used in the current version of the algorithm/tester
   * @return mutated failure settings for another test
   */
  @Override
  public SchedulerSettings mutate() {
    int failureToRemove = random.nextInt(failures.size());

    List<NodeFailure> mutation = new ArrayList<>(failures);
    mutation.remove(failureToRemove);

    // add existing:
    int[] failurePerPhase = new int[conf.NUM_PHASES];
    List<Integer> phases = new ArrayList<>();

    for(int i = 0; i < conf.NUM_PHASES; i++) {
      phases.add(i);
    }

    for (NodeFailure nodeFailure : mutation) {
      int phaseToFailAt = nodeFailure.k;
      failurePerPhase[phaseToFailAt]++;
      if (failurePerPhase[phaseToFailAt] == conf.NUM_PROCESSES)
        phases.remove(phaseToFailAt);
    }

    int phaseToFailAt = random.nextInt(phases.size());
    int roundToFailAt = random.nextInt(conf.NUM_ROUNDS_IN_PROTOCOL);
    int processToFail = random.nextInt(conf.NUM_PROCESSES);

    mutation.add(new NodeFailure(phaseToFailAt, roundToFailAt, processToFail));

    return new NodeFailureSettings(conf.getRandomSeed(), mutation, ++numMutators);
  }

  private List<NodeFailureSettings.NodeFailure> getRandomFailures() {
    if(depth > 0 )
      return getBoundedRandomFailures(depth);
    else
      return getUnboundedRandomFailures();
  }

  private List<NodeFailureSettings.NodeFailure> getBoundedRandomFailures(int d) {
    List<NodeFailureSettings.NodeFailure> f  = new ArrayList<>();

    int[] failurePerPhase = new int[conf.NUM_PHASES];
    List<Integer> phases = new ArrayList<>();

    for(int i = 0; i < conf.NUM_PHASES; i++) {
      phases.add(i);
    }

    for(int i = 0; i < d; i++) {
      int phaseToFailAt = random.nextInt(phases.size());
      failurePerPhase[phaseToFailAt] ++;
      if(failurePerPhase[phaseToFailAt] == conf.NUM_PROCESSES)
        phases.remove(phaseToFailAt);

      int roundToFailAt = random.nextInt(conf.NUM_ROUNDS_IN_PROTOCOL);
      int processToFail = random.nextInt(conf.NUM_PROCESSES);

      f.add(new NodeFailure(phaseToFailAt, roundToFailAt, processToFail));
    }

    return f;
  }

  private List<NodeFailureSettings.NodeFailure> getUnboundedRandomFailures() {
    List<NodeFailureSettings.NodeFailure> f  = new ArrayList<>();

    // for each phase, select a set of processes to fail, at a selected round
    for(int i = 0; i < conf.NUM_PHASES; i++) {
      for(int j = 0; j < conf.NUM_PROCESSES; j++) {
        if(random.nextDouble() > 0) {
          int roundNum = random.nextInt(6);
          f.add(new NodeFailure(i, roundNum, j));
        }
      }
    }
    return f;
  }

  public List<NodeFailureSettings.NodeFailure> getFailures() {
    return failures;
  }

  public String toString() {
    String sb = "Num processes: " + conf.NUM_PROCESSES + "\n" +
        "Num rounds in the protocol: " + conf.NUM_ROUNDS_IN_PROTOCOL + "\n" +
        "Num requests/phases: " + conf.NUM_PHASES + "\n" +
        "Link establishment period: " + conf.linkEstablishmentPeriod + "\n" +
        "Random seed: " + conf.getRandomSeed() + "\n" +
        "Bug depth: " + conf.bugDepth + "\n";
    return sb;
  }

  private List<NodeFailureSettings.NodeFailure> getFailuresToReproduceBug() {
    // depth is 6
    List<NodeFailureSettings.NodeFailure> f  = new ArrayList<>();
    f.add(new NodeFailure(0, 4, 2));
    f.add(new NodeFailure(1, 2, 2));
    f.add(new NodeFailure(1, 4, 0));
    f.add(new NodeFailure(2, 0, 0));
    f.add(new NodeFailure(2, 0, 1));
    f.add(new NodeFailure(3, 0, 0));
    return f;
  }

  public static class NodeFailure {
    @Expose
    int k; // in which request does it happen?
    @Expose
    int r; // at which round does it happen?
    @Expose
    int process; // which process fails?

    public NodeFailure(int k, int r, int p) {
      this.k = k;
      this.r = r;
      this.process = p;
    }

    @Override
    public boolean equals(Object obj) {
      if(!(obj instanceof NodeFailure)) return false;

      return k == ((NodeFailure)obj).k
          && r == ((NodeFailure)obj).r
          && process == ((NodeFailure)obj).process;
    }

    @Override
    public int hashCode() {
      int result = 17;
      result = 31 * result + k;
      result = 31 * result + r;
      result = 31 * result + process;
      return result;
    }

    public String toString() {
      return "k:" + k + " r:" + r + " proc:" + process;
    }
  };




}
