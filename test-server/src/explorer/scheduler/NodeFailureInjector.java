package explorer.scheduler;

import explorer.Event;
import explorer.ExplorerConf;
import explorer.message.PaxosMessage;
import explorer.schedule.EventListSchedule;
import explorer.schedule.Schedule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * The NodeFailureInjector injects failures into the synchronous non-faulty executions of a system
 * (The current version introduces only node failures where a failing node cannot receive/send any messages)
 */
public class NodeFailureInjector extends Scheduler {
  private static final Logger log = LoggerFactory.getLogger(NodeFailureInjector.class);
  ExplorerConf conf = ExplorerConf.getInstance();

  // variables maintaining the current state of the protocol execution
  private int currentRound;     // between 0 to (NUM_LIVENESS_ROUNDS-1)
  private int currentPhase;    // between 0 to (NUM_PHASES - 1)
  private int toExecuteInCurRound;
  private int executedInCurRound;
  private int droppedFromNextRound; // incremented for the response events in case request events are dropped

  private final int period;  // period of clearing failed processes (set to NUM_LIVENESS_ROUNDS)

  private List<NodeFailureSettings.NodeFailure> failures;
  private Set<Integer> failedProcesses;

  private List<PaxosMessage.ProtocolRound> rounds;
  protected int numTotalRounds; // the current number of effective rounds (together with the empty rounds with no quorums)

  // for stats:
  int numSuccessfulRounds = 0, numSuccessfulPhases = 0;

  public NodeFailureInjector(NodeFailureSettings settings) {
    this.settings = settings;
    period = conf.linkEstablishmentPeriod;
    initStructures();
  }

  public void initStructures() {
    failures = new ArrayList<>(((NodeFailureSettings)settings).getFailures());
    failedProcesses = new HashSet<>();
    rounds = new ArrayList<>();
    rounds.add(PaxosMessage.ProtocolRound.PAXOS_PREPARE);
    toExecuteInCurRound = conf.NUM_PROCESSES;
    currentRound = 0;
    currentPhase = 0;
    executedInCurRound = 0;
    droppedFromNextRound = 0;
    numTotalRounds = 0;

    log.debug("Using failures: " + ((NodeFailureSettings)settings).getFailures() + " seed: " + ((NodeFailureSettings)settings).seed);
  }

  @Override
  synchronized public void addNewEvents(List<Event> events) {
    super.addNewEvents(events);
  }

  @Override
  synchronized public Event scheduleNext() {
    List<Event> toScheduleClone = new ArrayList<>(toSchedule);
    for(Event m: toScheduleClone) {
      if(isOfCurrentRound(m.getMessage())) {
        if(isToDrop(m.getMessage())) {
          log.info("Dropped message: " + m.toString() + " " + m.getMessage().getPayload());
          drop(m);
          toExecuteInCurRound --;
          if(m.getMessage().isRequest()) droppedFromNextRound ++;
          checkUpdateRound();
        } else {
          log.debug("=== Scheduling:  " + m);
          schedule(m);
          executedInCurRound ++;
          checkUpdateRound();
          return m;
        }
      }
    }
    return null;
  }

  // The current phase is determined by the ballot numbers and round of the protocol step
  // After the PREPARE of the first request,
  //   all the messages regarding that request are executed before SCHEDULING the PREPARE of another one
  // has side effect!
  synchronized private boolean isToDrop(PaxosMessage message) {
    assert(isOfCurrentRound(message));
    int processOfMessage = (int)(message.isRequest() ? message.getRecv() : message.getSender());
    if(failedProcesses.contains(processOfMessage)) return true;

    NodeFailureSettings.NodeFailure match = null;
    for(NodeFailureSettings.NodeFailure nf: failures) {
      if(nf.k == currentPhase && rounds.get(currentRound).ordinal() == nf.r && nf.process == processOfMessage) {
        match = nf;
        break;
      }
    }

    if(match != null) {
      failures.remove(match);
      failedProcesses.add(match.process);
      return true;
    }

    return false;
  }

  // the standard algorithm increases the rounds by just collecting messages sent in the round - this is an optimization
  synchronized private void checkUpdateRound() {
    if((toExecuteInCurRound - executedInCurRound) == 0) { // move to next round
      currentRound ++;
      numTotalRounds ++;
      if(executedInCurRound >= ((NodeFailureSettings)settings).NUM_MAJORITY) numSuccessfulRounds ++;

      // update the next round - state machine
      switch(rounds.get(currentRound-1)) {
        case PAXOS_PREPARE:
          if(toExecuteInCurRound < ((NodeFailureSettings)settings).NUM_MAJORITY) {
            rounds.add(PaxosMessage.ProtocolRound.PAXOS_PREPARE_RESPONSE);
            toExecuteInCurRound = conf.NUM_PROCESSES - droppedFromNextRound;
          } else {
            rounds.add(PaxosMessage.ProtocolRound.PAXOS_PREPARE_RESPONSE);
            toExecuteInCurRound = conf.NUM_PROCESSES - droppedFromNextRound;
          }
          break;
        case PAXOS_PREPARE_RESPONSE:
          if(toExecuteInCurRound < ((NodeFailureSettings)settings).NUM_MAJORITY) {
            rounds.add(PaxosMessage.ProtocolRound.PAXOS_PREPARE);
            numTotalRounds += 4;
            currentPhase ++;
            toExecuteInCurRound = conf.NUM_PROCESSES;
          } else {
            rounds.add(PaxosMessage.ProtocolRound.PAXOS_PROPOSE);
            toExecuteInCurRound = conf.NUM_PROCESSES;
          }
          break;
        case PAXOS_PROPOSE: //fixed:
          if(toExecuteInCurRound < ((NodeFailureSettings)settings).NUM_MAJORITY) {
            rounds.add(PaxosMessage.ProtocolRound.PAXOS_PROPOSE_RESPONSE);
            toExecuteInCurRound = conf.NUM_PROCESSES - droppedFromNextRound;
          } else {
            rounds.add(PaxosMessage.ProtocolRound.PAXOS_PROPOSE_RESPONSE);
            toExecuteInCurRound = conf.NUM_PROCESSES - droppedFromNextRound;
          }
          break;
        case PAXOS_PROPOSE_RESPONSE: //todo make it more accurate with replies! (even with majority of replies, we can turn back to PREPARE)
          if(toExecuteInCurRound < ((NodeFailureSettings)settings).NUM_MAJORITY) {
            rounds.add(PaxosMessage.ProtocolRound.PAXOS_PREPARE);
            numTotalRounds += 2;
            currentPhase ++;
          }
          else rounds.add(PaxosMessage.ProtocolRound.PAXOS_COMMIT);
          toExecuteInCurRound = conf.NUM_PROCESSES;
          break;
        case PAXOS_COMMIT:
          if(toExecuteInCurRound < ((NodeFailureSettings)settings).NUM_MAJORITY) {
            rounds.add(PaxosMessage.ProtocolRound.PAXOS_COMMIT_RESPONSE);
            toExecuteInCurRound = conf.NUM_PROCESSES - droppedFromNextRound;
          } else {
            rounds.add(PaxosMessage.ProtocolRound.PAXOS_COMMIT_RESPONSE);
            toExecuteInCurRound = conf.NUM_PROCESSES - droppedFromNextRound;
            numSuccessfulPhases ++;
          }
          break;
        case PAXOS_COMMIT_RESPONSE:
          rounds.add(PaxosMessage.ProtocolRound.PAXOS_PREPARE);
          toExecuteInCurRound = conf.NUM_PROCESSES;
          currentPhase ++;
          break;
        default:
          log.error("Invalid protocol state");
      }

      // update values related to failures for the current round:
      executedInCurRound = 0;
      droppedFromNextRound = 0;

      // reset the quorum after each period number of rounds
      if(numTotalRounds % period == 0)
        failedProcesses.clear();

    }
  }

  // Customized for Cassandra example - the default way of detecting the messages in a round is to collect messages for some timeout
  synchronized private boolean isOfCurrentPhase(PaxosMessage m) {
    boolean isPrepareOfCurrentPhase = (m.getVerb().equals(PaxosMessage.ProtocolRound.PAXOS_PREPARE.toString()) && m.getPayloadBallot().equals(ballots.get(currentPhase)));
    boolean isNonPrepare = !m.getVerb().equals(PaxosMessage.ProtocolRound.PAXOS_PREPARE.toString()); // if the preceding messages are scheduled, it is of current phase
    return isPrepareOfCurrentPhase || isNonPrepare;
  }

  // Customized for Cassandra example - the default way of detecting the messages in a round is to collect messages for some timeout
  synchronized private boolean isOfCurrentRound(PaxosMessage m) {
      return isOfCurrentPhase(m) && m.getVerb().equals(rounds.get(currentRound).toString());
  }

  @Override
  public boolean isScheduleCompleted() {
    // executions that completed max number of rounds are completed
    if(numTotalRounds >= conf.NUM_MAX_ROUNDS) {
      log.info("Hit the max number of rounds, returning.");
      return true;
    }

    if(numSuccessfulPhases == conf.NUM_REQUESTS) {
      log.info("Hit the max number of phases, returning.");
      return true;
    }

    return false;
  }

  public String getFailuresAsStr() {
    return ((NodeFailureSettings)settings).getFailures().toString();
  }

  @Override
  public String getStats() {
    StringBuilder sb = new StringBuilder();
    sb.append("Num successful rounds: ").append(numSuccessfulRounds).append("\n");
    sb.append("Num rounds: ").append(numTotalRounds).append("\n");
    sb.append("Num successful phases: ").append(numSuccessfulPhases).append("\n");
    sb.append("Num phases: ").append(currentPhase).append("\n");
    sb.append("Num messages: ").append(scheduled.size()).append("\n");
    sb.append(super.getStats());
    sb.append("Failures: ").append(getFailuresAsStr());
    return sb.toString();
  }

  @Override
  public Schedule getSchedule() {
    return new EventListSchedule(getScheduledMessages());
  }

}
