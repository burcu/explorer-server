package explorer.scheduler;

import explorer.Event;
import explorer.ExplorerConf;
import explorer.message.PaxosMessage;
import explorer.schedule.PCTSchedule;
import explorer.schedule.Schedule;
import explorer.scheduler.pct.Chain;
import explorer.scheduler.pct.ChainPartitioner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;


/**
 * For now, assumes all messages are delivered (no message drops)
 */
public class PCTScheduler extends Scheduler {
  private static final Logger log = LoggerFactory.getLogger(PCTScheduler.class);
  ExplorerConf conf = ExplorerConf.getInstance();

  private final PCTSettings settings;
  private Random random;

  private ChainPartitioner cp;

  private int numPrChangePts;
  private List<Integer> prChangePts; // assigned randomly
  private final List<String> prChangeEventIds = new ArrayList<>();

  // Contains chains in the order of priorities --> higher indexed chains have higher priorities
  // The first numPrChangePts chains are reserved for the reduced priority chains (initially no chain, i.e., id -1)
  private List<Long> priorityChains;
  private List<Integer> priorityChainPositionsInserted; // collects random positions generated - to save the schedule

  //private List<Long> scheduledChainIds;
  private Set<String> priorityReducedAt;
  private int maxNumAvailableChains = 0; // for stats
  private List<String> messagesStronglyHit; // for stats

  public PCTScheduler(PCTSettings settings) {
    this.settings = settings;
    initStructures();
  }

  private void initStructures() {
    cp = new ChainPartitioner();
    random = new Random(settings.getRandomSeed());
    // bugDepth #events to order, bugDepth - 1 #priority-change-points in PCTCPScheduler
    numPrChangePts = settings.getBugDepth() - 1;
    prChangePts = new ArrayList<>();

    priorityChains = new ArrayList<>();
    for(int i = 0; i < numPrChangePts; i++)
      priorityChains.add(-1L);

    priorityReducedAt = new HashSet<>();
    messagesStronglyHit = new ArrayList<>();

    for(int i = 0; i < numPrChangePts; i++) {
      prChangePts.add(random.nextInt(settings.getWindowEndIndex() - settings.getWindowStartIndex()) + settings.getWindowStartIndex());
    }

    for(int index: prChangePts) {
      prChangeEventIds.add(PaxosMessage.messageIds.get(index));
    }

    priorityChainPositionsInserted = new ArrayList<>();
  }

  @Override
  public void addNewEvents(List<Event> events) {
    super.addNewEvents(events);

    for(Event e: events) {
      if(!cp.hasCurrentEvent(e.getMessage().getMessageContentWithBallot())) { // compare to id without seq number
        cp.insert(e);
      }
    }

    // returns all the current chains
    Set<Long> newChains = new HashSet<>(cp.getChainIds());
    Set<Long> prevChains = new HashSet<>(priorityChains);
    newChains.removeAll(prevChains);

    // assign a random priority for each newly added chain
    for (Long chain : newChains) {
      int pos = random.nextInt(priorityChains.size() - numPrChangePts + 1) + numPrChangePts - 1;
      priorityChainPositionsInserted.add(pos);
      priorityChains.add(pos, chain);
    }

    // update maxNumAvailableChains for stats
    int numAvailableChains = getNumEnabledChains();
    if(maxNumAvailableChains < numAvailableChains)
      maxNumAvailableChains = numAvailableChains;
  }

  // for stats
  private int getNumEnabledChains() {
    int count = 0;
    for(Chain c: cp.getChains()) {
      if(c.hasEventToConsume()) count ++;
    }
    return count;
  }


  @Override
  public Event scheduleNext() {
    Chain chain = getCurrentChain();
    if (chain == null) {
      log.debug("Current chain is null. \n");
      //System.exit(-1);
      return null;
    }
    Event eventToSchedule = chain.consume();
    if(eventToSchedule == null) {
      log.error("Null node to schedule\n" );
      System.exit(-1);
    }
    //scheduledChainIds.add(chain.getId());
    //log.debug("Scheduling chain: " + chain.getId());
    schedule(eventToSchedule);
    return eventToSchedule;
  }


  /**
   * Finds the chain to be scheduled next
   * @return chain id of the current chain, -1 id none
   */
  private Chain getCurrentChain() {
    for(int i = priorityChains.size()-1; i >= 0; i--) {
      long chainId = priorityChains.get(i);
      if(chainId > 0) {
        Chain c = cp.getChainById(chainId);
        if(c.hasEventToConsume()) return checkForPriorityChange(c);
      }
    }
    return null;
  }

  private Chain checkForPriorityChange(Chain currentChain) {
    Event event = currentChain.getEventToConsume();
    String eventId = event.getMessage().getMessageContentWithBallot();

    if(prChangeEventIds.contains(eventId) && !priorityReducedAt.contains(eventId)) {
      int index = prChangeEventIds.indexOf(eventId);
      reducePriority(currentChain, index);
      priorityReducedAt.add(eventId);
      log.debug("--Reducing priority at msg: " + eventId);
      messagesStronglyHit.add(event.getMessage().toString());
      currentChain = getCurrentChain();
      assert(currentChain != null);
    }
    return currentChain;
  }

  /**
   * Reduce priority of the given chain, based on its index value
   * @param currentChain - the chain whose priority will be reduced
   * @param index - index of the current priority change point in the tuple of priority changes
   */
  private void reducePriority(Chain currentChain, int index) {
    int prevIndex = priorityChains.indexOf(currentChain.getId());

    if(prevIndex >= numPrChangePts) { // high priority chain is getting reduced
      priorityChains.remove(currentChain.getId());
    } else { // reduced priority chain is getting reduced
      priorityChains.set(prevIndex, -1L);
    }

    priorityChains.set(numPrChangePts - index - 1, currentChain.getId()); // preserve order!

    //logger.debug("Chains ordered by priority after: ");
    //printPriorities();
  }

  public void printPriorities() {
    StringBuilder sb = new StringBuilder("Priorities with increasing indices (priorities): \n");
    sb.append("HIGH: (from lowest to highest order)");
    for(int i = priorityChains.size()-1; i >= numPrChangePts; i--) {
      sb.append(priorityChains.get(i)).append(" ");
    }

    sb.append(" + \nREDUCED: (from highest to lowest order)");

    for(int i = numPrChangePts - 1; i >= 0; i--) {
      if(priorityChains.get(i) > 0)
        sb.append(priorityChains.get(i)).append(" ");
    }
    log.info(sb.toString());
  }

  @Override
  public String getStats() {
    return "PCT with: " + "Seed: " + settings.getRandomSeed() + "  " +
            "\nPriority change points: " + prChangePts +//.append("\n");
            "Strongly hit messages:\n" + messagesStronglyHit() +
            "#scheduled: " + getScheduledMessages().size() +
            " " + super.getStats();
  }

  private String messagesStronglyHit() {
    String s = "";
    for(String m: messagesStronglyHit)
      s = s.concat("\t").concat(m).concat("\n");
    return s;
  }

  @Override
  public synchronized boolean isScheduleCompleted() {
    return scheduled.size() == conf.NUM_MAX_MESSAGES;
  }

  @Override
  public Schedule getSchedule() {
    return new PCTSchedule(priorityChainPositionsInserted, prChangePts);
  }
}
