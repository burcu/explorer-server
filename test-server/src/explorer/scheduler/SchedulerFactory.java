package explorer.scheduler;

import explorer.ExplorerConf;

import java.lang.reflect.InvocationTargetException;

public class SchedulerFactory {
  /**
   * Creates and returns a Scheduler of type specified in configuration
   * @param conf ExplorerConf that keeps scheduler and test configuration
   * @param testId used to obtain the random seed for the test by adding it to the random seed in configuration
   * @return
   */
  public static Scheduler createScheduler(ExplorerConf conf, int testId) {
    try {
      Scheduler scheduler = null;
      SchedulerSettings settings;
      Class<? extends Scheduler> schedulerClass = (Class<? extends Scheduler>) Class.forName(conf.getSchedulerClass());
      conf.setRandomSeed(conf.getRandomSeed() + testId);

      switch(conf.getSchedulerClass()) {
        case "explorer.scheduler.NaiveRandomFailureInjector":
          scheduler = schedulerClass.getConstructor().newInstance();
          break;
        case "explorer.scheduler.NodeFailureInjector":
          settings = new NodeFailureSettings(conf.getRandomSeed());
          scheduler = schedulerClass.getConstructor(NodeFailureSettings.class).newInstance(settings);
          break;
        case "explorer.scheduler.LinkFailureInjector":
          settings = new LinkFailureSettings(conf.getRandomSeed());
          scheduler = schedulerClass.getConstructor(LinkFailureSettings.class).newInstance(settings);
          break;
        case "explorer.scheduler.ReplayingScheduler":
          scheduler = schedulerClass.getConstructor(String.class).newInstance(conf.schedulerFile);
          break;
        case "explorer.scheduler.PCTScheduler":
          settings = new PCTSettings(conf);
          scheduler = schedulerClass.getConstructor(PCTSettings.class).newInstance(settings);
          break;
        case "explorer.scheduler.POSScheduler":
          scheduler = schedulerClass.getConstructor().newInstance();
          break;
      }
      return scheduler;
    } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException
        | InvocationTargetException | InstantiationException e) {
      e.printStackTrace();
      System.exit(-1);
      return null;
    }
  }
}
