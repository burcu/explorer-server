package explorer.scheduler;

import explorer.Event;
import explorer.schedule.Schedule;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Drops messages arbitrarily - at random with given probability
 */
public class NaiveRandomFailureInjector extends Scheduler {
    private Random random;
    public final int NUM_MAJORITY= (conf.NUM_PROCESSES / 2) + 1;

    private HashMap<String, Integer> numCommitsPerBallot;

    public NaiveRandomFailureInjector() {
        initStructures();
    }

    public void initStructures() {
        random = new Random(conf.getRandomSeed());
        numCommitsPerBallot = new HashMap<>();
    }

    @Override
    synchronized public void addNewEvents(List<Event> events) {
        super.addNewEvents(events);
    }

    private int getNumSuccessfulCommits() {
        int numSuccessful = 0;
        for(int i: numCommitsPerBallot.values()) {
            if(i >= NUM_MAJORITY) numSuccessful ++;
        }
        return numSuccessful;
    }

    @Override
    public Event scheduleNext() {
        if(toSchedule.isEmpty()) {
            return null;
        }

        List<Event> messagesClone = new ArrayList<>(toSchedule);
        for(Event m: messagesClone) {
            if(random.nextDouble() < conf.dropWithProb) {
                drop(m);
            } else {
                if(m.getMessage().getVerb().equals("PAXOS_COMMIT")) {
                    if(!numCommitsPerBallot.containsKey(m.getMessage().getPayloadBallot())) {
                        numCommitsPerBallot.put(m.getMessage().getPayloadBallot(), 1);
                    } else {
                        int count = numCommitsPerBallot.get(m.getMessage().getPayloadBallot());
                        numCommitsPerBallot.put(m.getMessage().getPayloadBallot(), count + 1);
                    }
                }
                schedule(m);
                return m;
            }
        }

        return null;
    }

    public boolean isScheduleCompleted() {
        int successful = getNumSuccessfulCommits();
        return successful >= conf.NUM_REQUESTS;
    }

    @Override
    public String getStats() {
        StringBuffer sb = new StringBuffer();
        sb.append("\nNum successful phases: ").append(getNumSuccessfulCommits()).append("\n");
        sb.append("Num messages: ").append(scheduled.size()).append("\n");
        sb.append("Num dropped messages: ").append(dropped.size()).append("\n");
        sb.append(super.getStats());
        return sb.toString();
    }

    @Override
    public Schedule getSchedule() {
        throw new RuntimeException("NaiveRandomFailureInjector does not yet support \"getSchedule\"");
    }

}
