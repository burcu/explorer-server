package explorer.scheduler;

import explorer.Event;
import explorer.ExplorerConf;
import explorer.message.PaxosMessage;
import explorer.schedule.PCTSchedule;
import explorer.schedule.Schedule;
import explorer.scheduler.pct.Chain;
import explorer.scheduler.pct.ChainPartitioner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;


/**
 * Replays a PCT execution, given chain priorities and priority change points (recorded and mutated)
 *    - Deviates from the parameters if a different set of chains are generated
 */
public class PCTReplayingScheduler extends Scheduler {
    private static final Logger log = LoggerFactory.getLogger(PCTScheduler.class);
    ExplorerConf conf = ExplorerConf.getInstance();

    private PCTSchedule pctSchedule;
    private Random random;
    private final int randomSeed;

    private ChainPartitioner cp;

    private int numPrChangePts;
    private List<Integer> prChangePts; // assigned randomly
    private final List<String> prChangeEventIds = new ArrayList<>();

    // Contains chains in the order of priorities --> higher indexed chains have higher priorities
    // The first numPrChangePts chains are reserved for the reduced priority chains (initially no chain, i.e., id -1)
    private List<Long> priorityChains;
    private List<Integer> priorityChainPositions; // collects random positions generated - to save the schedule
    private int numCreatedChains = 0;

    //private List<Long> scheduledChainIds;
    private Set<String> priorityReducedAt;
    private int maxNumAvailableChains = 0; // for stats
    private List<String> messagesStronglyHit; // for stats

    /**
     * Runs and UPDATES (if #chains differ from the initial # in pctSchedule) pctSchedule
     * @param pctSchedule Specifies #chain position (for assigning priorities) and (d-1) events
     * @param randomSeed to be used for assigning priority/position for new chains
     */
    public PCTReplayingScheduler(PCTSchedule pctSchedule, int randomSeed) {
        this.pctSchedule = pctSchedule;
        this.randomSeed = randomSeed;
        initStructures();
    }

    private void initStructures() {
        cp = new ChainPartitioner();
        random = new Random(randomSeed);
        // bugDepth #events to order, bugDepth - 1 #priority-change-points in PCTCPScheduler
        numPrChangePts = pctSchedule.getPriorityChangePoints().size();
        prChangePts = new ArrayList<>(pctSchedule.getPriorityChangePoints());

        priorityChains = new ArrayList<>();
        for(int i = 0; i < numPrChangePts; i++) {
            priorityChains.add(-1L);
        }

        //scheduledChainIds = new ArrayList<Long>();
        priorityReducedAt = new HashSet<>();
        messagesStronglyHit = new ArrayList<>();

        for(int index: prChangePts) {
            prChangeEventIds.add(PaxosMessage.messageIds.get(index));
        }

        priorityChainPositions = new ArrayList<>();
    }

    @Override
    public void addNewEvents(List<Event> events) {
        super.addNewEvents(events);

        for(Event e: events) {
            if(!cp.hasCurrentEvent(e.getMessage().getMessageContentWithBallot())) { // compare to id without seq number
                cp.insert(e);
            }
        }

        // returns all the current chains
        Set<Long> newChains = new HashSet<>(cp.getChainIds());
        Set<Long> prevChains = new HashSet<>(priorityChains);
        newChains.removeAll(prevChains);

        // assign a random priority for each newly added chain
        for (Long chain : newChains) {
            // instead of inserting into random positions, read the generated numbers from pctSchedule
            // int pos = random.nextInt(priorityChains.size() - numPrChangePts + 1) + numPrChangePts - 1;
            // if original schedule had smaller number of chains, generate a new one
            int pos;
            if(pctSchedule.getPriorityChainPositions().size() <= numCreatedChains) {
                pos = random.nextInt(priorityChains.size() - numPrChangePts + 1) + numPrChangePts - 1;
            } else {
                pos = pctSchedule.getPriorityChainPositions().get(numCreatedChains++);
            }
            priorityChainPositions.add(pos);
            priorityChains.add(pos, chain);
        }
        // update maxNumAvailableChains for stats
        int numAvailableChains = getNumEnabledChains();
        if(maxNumAvailableChains < numAvailableChains)
            maxNumAvailableChains = numAvailableChains;
    }

    // for stats
    private int getNumEnabledChains() {
        int count = 0;
        for(Chain c: cp.getChains()) {
            if(c.hasEventToConsume()) count ++;
        }
        return count;
    }


    @Override
    public Event scheduleNext() {
        Chain chain = getCurrentChain();
        if (chain == null) {
            log.debug("Current chain is null. \n");
            //System.exit(-1);
            return null;
        }
        Event eventToSchedule = chain.consume();
        if(eventToSchedule == null) {
            log.error("Null node to schedule\n" );
            System.exit(-1);
        }
        //scheduledChainIds.add(chain.getId());
        //log.debug("Scheduling chain: " + chain.getId());
        schedule(eventToSchedule);
        return eventToSchedule;
    }


    /**
     * Finds the chain to be scheduled next
     * @return chain id of the current chain, -1 id none
     */
    private Chain getCurrentChain() {
        for(int i = priorityChains.size()-1; i >= 0; i--) {
            long chainId = priorityChains.get(i);
            if(chainId > 0) {
                Chain c = cp.getChainById(chainId);
                if(c.hasEventToConsume()) return checkForPriorityChange(c);
            }
        }
        return null;
    }

    private Chain checkForPriorityChange(Chain currentChain) {
        Event event = currentChain.getEventToConsume();
        String eventId = event.getMessage().getMessageContentWithBallot();

        if(prChangeEventIds.contains(eventId) && !priorityReducedAt.contains(eventId)) {
            int index = prChangeEventIds.indexOf(eventId);
            reducePriority(currentChain, index);
            priorityReducedAt.add(eventId);
            log.debug("--Reducing priority at msg: " + eventId);
            messagesStronglyHit.add(event.getMessage().toString());
            currentChain = getCurrentChain();
            assert(currentChain != null);
        }
        return currentChain;
    }

    /**
     * Reduce priority of the given chain, based on its index value
     * @param currentChain - the chain whose priority will be reduced
     * @param index - index of the current priority change point in the tuple of priority changes
     */
    private void reducePriority(Chain currentChain, int index) {
        int prevIndex = priorityChains.indexOf(currentChain.getId());

        if(prevIndex >= numPrChangePts) { // high priority chain is getting reduced
            priorityChains.remove(currentChain.getId());
        } else { // reduced priority chain is getting reduced
            priorityChains.set(prevIndex, -1L);
        }

        priorityChains.set(numPrChangePts - index - 1, currentChain.getId()); // preserve order!

        //logger.debug("Chains ordered by priority after: ");
        //printPriorities();
    }

    public void printPriorities() {
        StringBuilder sb = new StringBuilder("Priorities with increasing indices (priorities): \n");
        sb.append("HIGH: (from lowest to highest order)");
        for(int i = priorityChains.size()-1; i >= numPrChangePts; i--) {
            sb.append(priorityChains.get(i)).append(" ");
        }

        sb.append(" + \nREDUCED: (from highest to lowest order)");

        for(int i = numPrChangePts - 1; i >= 0; i--) {
            if(priorityChains.get(i) > 0)
                sb.append(priorityChains.get(i)).append(" ");
        }
        log.info(sb.toString());
    }

    @Override
    public String getStats() {

        return "PCT from a schedule with: " + "Given Seed: " + randomSeed + "\n" +
                "Given Priority change points: " + prChangePts + "\n" +
                "Chains are inserted into positions: " + priorityChainPositions + "\n" +
                "Strongly hit messages:\n" + messagesStronglyHit() +
                "#scheduled: " + getScheduledMessages().size() +
                " " + super.getStats();
    }

    private String messagesStronglyHit() {
        String s = "";
        for(String m: messagesStronglyHit)
            s = s.concat("\t").concat(m).concat("\n");
        return s;
    }

    @Override
    public synchronized boolean isScheduleCompleted() {
        boolean completed = scheduled.size() == conf.NUM_MAX_MESSAGES;
        if(completed) pctSchedule = new PCTSchedule(priorityChainPositions, prChangePts);
        return completed;
    }

    @Override
    public Schedule getSchedule() {
        return pctSchedule;
    }
}

