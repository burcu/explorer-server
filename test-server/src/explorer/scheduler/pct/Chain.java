package explorer.scheduler.pct;

import explorer.Event;

import java.util.ArrayList;
import java.util.List;

public class Chain {

  private int id;
  private List<Event> elems;
  private int lastConsumedEvent; // the index of the last consumed event

  public Chain(int id, List<Event> elems) {
    this.id = id;
    this.elems = elems;
    lastConsumedEvent = -1;
  }

  public long getId() {
    return id;
  }

  public boolean append(Event elem) {
    if(canAppend(elem)) {
      elems.add(elem);
      return true;
    }

    return false;
  }

  // return the most recently added element in the chain
  public Event getMaximal() {
    if(elems.isEmpty()) return null;
    return elems.get(elems.size()-1);
  }

  public List<Event> getEventsToConsume() {
    List<Event> nodes = new ArrayList<>();
    for(int i = lastConsumedEvent + 1; i < elems.size(); i++) {
      nodes.add(elems.get(i));
    }
    return nodes;
  }

  public boolean hasEventToConsume() {
    return elems.size() > lastConsumedEvent + 1;
  }

  public Event getEventToConsume() {
    if(hasEventToConsume()) {
      return elems.get(lastConsumedEvent +1);
    }
    return null;
  }

  // marks the next element as consumed
  public Event consume() {
    if(hasEventToConsume()) {
      lastConsumedEvent++;
      return elems.get(lastConsumedEvent);
    }
    return null;
  }

  public Event getEvent(int index) {
    if(index >= elems.size()) return null;
    return elems.get(index);
  }

  public boolean isConsumed(int index) {
    return lastConsumedEvent >= index && index >= 0;
  }

  public String toString() {
    return concatNodeStrings(elems);
  }

  public String toConsumeToString() {
    return concatNodeStrings(getEventsToConsume());
  }

  private String concatNodeStrings(List<Event> events) {
    StringBuilder sb = new StringBuilder("Chain " + id + ": [ ");

    for(int i=0; i<events.size(); i++) {
      Event e = events.get(i);
      sb.append(e.getMessage().getMessageContentWithSeqNumber());
      if(isConsumed(i)) sb.append("(C)");
      sb.append("  ");
    }
    return sb.append("]").toString();
  }

  //todo add hack for Cassandra - if the last transition is Paxos-Prepare and not executed, the next is concurrent to this - not appendable
  public boolean canAppend(Event e) {

    // if the chain has a single node, which is not executed yet and it is a client request, then it is concurrent to the other messages
    //if(elems.size() == 1 && hasNodeToConsume() && elems.get(0).initiatedByClient()) return false;
    //if(elem.initiatedByClient()) return false;
    // if elems.get(elems.size() - 1).getEvent() happens before elem, returns 1
   // return VectorClockUtil.isConcurrent(elem.getEvent(), getMaximal().getEvent()) == 1 && !hasNodeToConsume();
    //  return VectorClockUtil.isConcurrent(elem.getEvent(), getMaximal().getEvent()) == 1 &&
    //    (!hasNodeToConsume() || Node.hasSameClientRequest(elem,  getMaximal()));
/*    return (
        (Node.hasSameSRPair(elem,  getMaximal()) &&
        (!hasNodeToConsume() || Node.hasSameClientRequest(elem,  getMaximal())))
        ||
        (Node.hasReverseSRPair(elem,  getMaximal()) && // the second is an answer
        (!hasNodeToConsume() || Node.hasSameClientRequest(elem,  getMaximal()))));
  */
    return
        (e.getMessage().hasSameSRPair(getMaximal().getMessage())) && // same sender receiver AND
            // no event to consume OR Same request..
            (!hasEventToConsume() || (e.getMessage().hasSameClientRequest(getMaximal().getMessage())));
  
    //return !hasNodeToConsume(); 

 }


}
