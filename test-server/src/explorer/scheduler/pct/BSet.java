package explorer.scheduler.pct;

import explorer.Event;

import java.util.*;

public class BSet {
  private List<Chain> chains;
  private int capacity;

  public BSet(int capacity) {
    chains = new ArrayList<>();
    this.capacity = capacity;
  }

  public BSet(int capacity, Chain c) {
    chains = new ArrayList<>();
    chains.add(c);
    this.capacity = capacity;
  }

  public BSet(int capacity, List<Chain> chains) {
    this.chains = chains;
    this.capacity = capacity;
  }

  public BSet(List<Chain> c) {
    chains = c;
  }

  public List<Chain> getChains() {
    return chains;
  }

  public int getCapacity() {
    return capacity;
  }

  public int getNumChains() {
    return chains.size();
  }


  public int size() {
    return chains.size();
  }

  public List<Chain> getAppendableChains(Event event) {
    List<Chain> appendable = new ArrayList<Chain>();

    for(Chain c: chains)
      if(c.canAppend(event)) appendable.add(c);

    return appendable;
  }

  public void addChain(Chain c) {
    chains.add(c);
    assert(chains.size() <= capacity);
  }

  public boolean hasComparableMaximals() {
    List<Event> heads = new ArrayList<>();

    for(Chain c: chains) {
      Event head = c.getMaximal();
      if(head != null) heads.add(head);
    }

    for(int i = 0; i < heads.size(); i++) {
      for(int j = 0; j < i; j++) {
        if(heads.get(i).isConcurrentTo(heads.get(j)))
          return true;
      }
    }
    return false;
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    for(Chain c: chains) {
      sb.append(c.toString());
      sb.append("\n");
    }
    return sb.toString();
  }
}
