package explorer.scheduler.pct;

import explorer.Event;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class ChainPartitioner {
  protected static Logger logger = LoggerFactory.getLogger(ChainPartitioner.class);

  private Partitioning partitioning = new Partitioning();

  public void insert(Event event) {
    partitioning.insert(event);
  }

  // IMPORTANT: transition id hashes collide!
  // Instead of this, check a new transition with the not-executed transitions only (using hasCurrentTransitions)
  public boolean hasEvent(String transitionId) {
    return partitioning.hasEvent(transitionId);
  }

  public boolean hasCurrentEvent(String transitionId) {
    return partitioning.hasCurrentEvent(transitionId);
  }

  public List<Chain> getChains() {
    return partitioning.getChains();
  }

  public List<Long> getChainIds() {
    List<Chain> chains = getChains();
    List<Long> ids = new ArrayList<Long>();
    for(Chain c: chains)
      ids.add(c.getId());
    return ids;
  }

  public Chain getChainById(long chainId) {
    return partitioning.getChainById(chainId);
  }

  public Chain getChainByHeadEventId(String eventId) {
    return partitioning.getChainByHeadEventId(eventId);
  }

  public Chain getChainByHeadTransitionId(String transitionId) {
    return partitioning.getChainByHeadEventId(transitionId);
  }

  @Override
  public String toString() {
    return partitioning.toString();
  }
}
