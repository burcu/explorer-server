package explorer.scheduler;

import explorer.Event;
import explorer.ExplorerConf;
import explorer.message.PaxosMessage;
import explorer.net.MessageSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Communicates to the ConnectionHandler and keeps MessageSenders of the processes
 * Sends received events from ConnectionHandler to Scheduler and
 * Sends the event to be scheduled next from Scheduler to ConnectionHandler
 */
public class SchedulerProxy {
  private static Logger log = LoggerFactory.getLogger(SchedulerProxy.class);
  private ExplorerConf conf = ExplorerConf.getInstance();

  private Scheduler scheduler;

  /**
   * Communication with the system under test - system messages in form of PaxosMessage are collected
   * */
  protected ConcurrentHashMap<Integer, MessageSender> messageSenders; //connectionId to sender
  protected ConcurrentHashMap<PaxosMessage, Integer> events; // event to message sender map
  // synchronize when the messages are done on the receiver side : <receiverId, the list of messages sent to it>
  // once the receiver is free (sends and ACK), allow the next message to be sent to it
  // to allow this, we send the message to the sender of the onflight message!
  private ConcurrentHashMap<Integer, Queue<PaxosMessage>> onFlightToReceiver;
  // maps the onflight messages to its message senders
  private ConcurrentHashMap<PaxosMessage, Integer> onFlightMsgSenders;

  private int numMessagesReceived; // assign sequence id to messages

  /**
   * Communication with the scheduler - a PaxosMessage is wrapped in an Event, together with its vector clock
   * */
  // messages collected as response to scheduling a message
  private List<Event> messages;
  private Map<PaxosMessage, String> messageBallots;
  private String lastScheduledBallot;

  // Structures for processing predecessors/causal dependencies of the messages
  private SortedMap<String, List<String>> messagePredecessors;
  private Map<Integer, List<String>> messagesDelivered;
  private Map<Integer, VectorClock> vectorClocks;
  private Map<String, VectorClock> msgVectorClocks;

  public SchedulerProxy() {
  }

  public void init(Scheduler scheduler) {
    this.scheduler = scheduler;
    resetStructures();
  }

  private synchronized void resetStructures() {
    messageSenders = new ConcurrentHashMap<Integer, MessageSender>();
    events = new ConcurrentHashMap<PaxosMessage, Integer>();
    onFlightToReceiver = new ConcurrentHashMap<Integer, Queue<PaxosMessage>>();
    onFlightMsgSenders = new ConcurrentHashMap<PaxosMessage, Integer>();
    messages = new ArrayList<>();
    messageBallots = new ConcurrentHashMap<PaxosMessage, String>();
    lastScheduledBallot = "";
    numMessagesReceived = 0;

    // for keeping track of event dependencies
    vectorClocks = new HashMap<>();
    msgVectorClocks = new HashMap<>();
    // for debugging purposes
    messagePredecessors = new TreeMap<String, List<String>>();
    messagesDelivered = new HashMap<>();
  }

  public final synchronized void onConnect(int connectionId, MessageSender sender) {
    messageSenders.put(connectionId, sender);
    onFlightToReceiver.put(connectionId - 1, new LinkedBlockingDeque<>()); // receiver ids vary from 0 to connectionId-1

    messagesDelivered.put(connectionId-1, new ArrayList<>());
    vectorClocks.put(connectionId-1, new VectorClock(conf.NUM_PROCESSES));
  }

  public final synchronized void onDisconnect(int id) {
    messageSenders.remove(id);
    //for(PaxosEvent e: events.keySet())
    //System.out.println(e);
  }

  public synchronized void addNewEvent(int connectionId, PaxosMessage message) {
    message.setSeqNumber(numMessagesReceived++);
    log.debug("Adding: " + message.getMessageContentWithSeqNumber());
    events.put(message, connectionId); // the intercepted message will be sent back to the connection

    VectorClock senderVC = vectorClocks.get((int)message.getSender()).clone();
    Event event = new Event(message, senderVC);
    messages.add(event);

    // set the ballot a message belongs to
    String ballot;
    if(message.getProtocolStep() == 0)
      ballot = message.getPayloadBallot();
    else
      ballot = lastScheduledBallot;

    message.setBallot(ballot);
    messageBallots.put(message, ballot); // need to add here to use with the message received from network (no ballot set)

    // the vector clock of the message is set to the vector clock of its sender
    msgVectorClocks.put(message.getMessageContentWithSeqNumber(), senderVC);
    // If a message mj is sent in a message mi, mi -> mj
    List<String> preds = new ArrayList<>(messagesDelivered.get((int)message.getSender()));
    // If a message mj is sent in a message mi, all messages m that are processed by mi's sender are also causally related
    messagePredecessors.put(message.getMessageContentWithSeqNumber(), preds);
  }

  public synchronized final void addAckEvent(int senderId, PaxosMessage message) {
    //System.out.println("Inside addAckEvent: senderId: " + senderId + " verb: " +  message.getVerb());
    // ACK event tells that the process has finished with the previous event
    // We can send the next event to the connection
    PaxosMessage e = onFlightToReceiver.get(senderId).remove();
    assert(e.getVerb().equals(message.getVerb()));

    // if the receiver has more messages, send them
    if(!onFlightToReceiver.get(senderId).isEmpty()) {
      PaxosMessage next = onFlightToReceiver.get(senderId).peek();
      int senderConId = onFlightMsgSenders.get(next);
      doSendToReceiverNode(next, senderConId);
    }
  }

  private synchronized void doSchedule(PaxosMessage message) {
    try{
      int connectionId = events.remove(message);
      lastScheduledBallot = messageBallots.get(message);
      sendToReceiverNode(message, connectionId);
      Thread.sleep(10);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private synchronized void sendToReceiverNode(PaxosMessage message, int connectionId) {
    onFlightToReceiver.get((int)message.getRecv()).add(message);
    onFlightMsgSenders.put(message, connectionId);
    if(onFlightToReceiver.get((int)message.getRecv()).size() == 1) {
      doSendToReceiverNode(message, connectionId);
    }
  }

  private synchronized void doSendToReceiverNode(PaxosMessage message, int connectionId) {
    //log.info("=== Scheduling:  " + message);
    String jsonStr = PaxosMessage.toJsonStr(message);
    MessageSender ms = messageSenders.get(connectionId);
    if(ms == null) // should not hit here
      log.error("Message sender for node " + message.getRecv() + " terminated." +
          "\nSkipped sending " + message.getMessageContentWithSeqNumber());
    else
      ms.send(jsonStr);
  }

  public synchronized boolean isScheduleCompleted() {
    // Bound the individual size
    if(scheduler.scheduled.size() >= conf.NUM_MAX_MESSAGES) return true;

    // update with dropped messages
    for(Event e: scheduler.dropped) {
      for(int i = 0; i < onFlightToReceiver.size(); i++) {
        onFlightToReceiver.get(i).remove(e.getMessage());
      }
    }

    boolean existsOnFlight = false;
    for(int i = 0; i < onFlightToReceiver.size(); i++) {
      existsOnFlight = existsOnFlight | !onFlightToReceiver.get(i).isEmpty();
    }

    return !existsOnFlight && scheduler.isScheduleCompleted();
  }

  // handling calls from controlled concurrency testing method:

  /**
   * Send the events/messages collected from the system to the scheduler
   */
  public synchronized void sendCollectedEvents() {
    scheduler.addNewEvents(new ArrayList<>(messages));
    messages.clear();
  }

  public synchronized boolean scheduleNext() {
    Event event = scheduler.scheduleNext();
    if(event != null) {
      String messageId = event.getMessage().getMessageContentWithSeqNumber();
      int receiver = (int)event.getMessage().getRecv();
      messagesDelivered.get(receiver).add(messageId);
      vectorClocks.get(receiver).updateWith(msgVectorClocks.get(messageId), receiver);
      doSchedule(event.getMessage());
      return true;
    }
    return false;
  }

  public List<PaxosMessage> getScheduledMessages() {
    return scheduler.getScheduledMessages();
  }
  public int numMessagesReceived() {
    return messages.size();
  }

  public int numMessagesScheduled() {
    return scheduler.scheduled.size();
  }
}
