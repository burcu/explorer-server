## Schedule Explorer for Distributed Systems

This repo contains code for the enforcement of certain schedules in the execution of [Cassandra distributed system](https://gitlab.mpi-sws.org/burcu/cas-6023). 


### Requirements:

- (Optional) For compiling Cassandra Cassandra 2.0.0
	- 	Java 7 (You can use jenv for supporting multiple java versions)
	-  Ant 1.9.14 (neededfor compiling Cassandra 2.0.0)

- For compiling the testing framework 
	-  Java >=8  

### Contents of the repo:
- ```test-server:``` Keeps the test server code which communicates the Cassandra nodes to intercept and enforce the processing of certain messages
- ```cassandra:``` Keeps the Cassandra libraries, configuration, data and query files to be used by Cassandra cluster nodes

### Installation of the test server 

Go to the server directory and build the explorer jar file:
 
```
cd test-server
./mvnw clean install
```

### (Optional) Installation of Cassandra 2.0.0

 instrumented Cassandra libraries You can build Cassandra linbraries by compiling the source code of Cassandra-2.0.0 which is instrumented to communicate to the test server: 

```
git clone https://gitlab.mpi-sws.org/burcu/cas-6023.git
```

Place the folder into ```explorer-server``` and compile the systems:

```
cd explorer-server/cassandra-6023
ant
```

Alternatively, you can use the prebuilt Cassandra libraries in ```cassandra/libs```.

<!--
### Replaying a schedule in Cassandra:

Configure the following parameters in ```explorer-server/test-server/explorer.conf``` file:

- The paths of directories/binaries
- The schedule file to reproduce


Run the exploration server. The server automatically starts the Cassandra nodes and sends query workloads to be processed.

```
java -jar target/test-server-jar-with-dependencies.jar 
```

Some example schedule files can be found in ```explorer-server/test-server/schedules``` folder.
-->


### Running  a test

You can run tests using custom schedulers by configuring the following parameters in ```explorer-server/test-server/explorer.conf``` file:

- (Necessary) The paths of directories and binaries
- (Optional) Exploration, Scheduler, test algorithm parameters
- (Optional) Timeouts, output files, etc.


The default algorithm parameters in the file runs a single single iteration of 5 tests using POSScheduler.

To run the test, execute the following command:

```
java -jar target/test-server-jar-with-dependencies.jar 
```

The output files are written to ```out``` folder. It contains a seperate output file for each iteration of tests, reporting the event priorities, the number of commits and the final values of the variables for each test.




